<?php
Class Info_member extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model('member_model');
        $this->load->model('info_member_model');
    }

    function index(){
        if(!mysession('id_user'))
            force_redirect('/login');
        
        // if(mysession("is_complete"))
        //    force_redirect('home/list');

        view_template("info_member",[
            "action_info" => site_url("info_member/insert_info")
        ]);
    }

    public function insert_info(){
        $photo = $this->info_member_model->select_file();
        $config = array(
                'upload_path'   =>  './assets/foto_member',
                'allowed_types' =>  'gif|jpg|jpeg|png|pdf',
                'encrypt_name'  =>  False,
                'max_size'      => '50000', // ini ukuran file gambar max nya
                'max_height'    =>  '9680',
                'max_width'     =>  '9024'
        );


        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        for ($x=1; $x <=2 ; $x++) { 
            if(!empty($_FILES['file_name'.$x]['name']))
            {
                if(!$this->upload->do_upload('file_name'.$x)):
                    echo  $this->upload->display_errors();  
                    exit;
                else:
                    $data_dok[] = array( "dok".$x => str_replace(" ", "_", $_FILES['file_name'.$x]['name']) );
                endif;
            }
        }

            $upload_data = $this->upload->data();
            if(isset($data_dok[0]['dok1'])){
                    $nama_file = $data_dok[0]['dok1'];

                    //image editornya
                $config['image_library']   = 'gd2';
                $config['source_image']    = './assets/foto_member/'.$data_dok[0]['dok1'];
                $config['new_image']       = './assets/foto_member/img';
                $config['create_thumb']    = TRUE;
                $config['quality']         = '100%';
                $config['maintain_ratio']  = TRUE;
                $config['width']           = 100; // ini satuanya pixel
                $config['height']          = 100; // ini juga pixel
                $config['x_axis']          = 0;
                $config['y_axis']          = 0;
                $config['thumb_marker']    = '';
                $this->image_lib->initialize($config);

                if(! $this->image_lib->resize())
                {
                    $data = $this->image_lib->display_errors();
                    //redirect();
                    exit;
                }
            }

          
                // unlink('./assets/foto_member/'.$photo->photo);
                // unlink('./assets/foto_member/img/'.$photo->photo);
                $data = array(
                    'id'                => mysession('id_user'),
                    'no_ktp'            =>  $this->input->post('no_ktp'),
                    'tempat_lahir'      =>  $this->input->post('tempat_lahir'),
                    'tanggal_lahir'     =>  $this->input->post('tanggal_lahir'),
                    'umur'              =>  $this->input->post('umur'),
                    'jenis_kelamin'     =>  $this->input->post('jenis_kelamin'),
                    'status'            =>  $this->input->post('status'),
                    'pendidikan'        =>  $this->input->post('pendidikan'),
                    'tahun_lulus'       =>  $this->input->post('tahun_lulus'),
                    'jurusan'           =>  $this->input->post('jurusan'),
                    'warga_negara'      =>  $this->input->post('warga_negara'),
                    'agama'             =>  $this->input->post('agama'),
                    'gol_darah'         =>  $this->input->post('gol_darah'),
                    'alamat_tetap'      =>  $this->input->post('alamat_tetap'),
                    'alamat_sekarang'   =>  $this->input->post('alamat_sekarang'),
                    'tinggi_badan'      =>  $this->input->post('tinggi_badan'),
                    'motivasi'      =>  $this->input->post('motivasi'),
                    'berat_badan'       =>  $this->input->post('berat_badan')
                );

                if(isset($data_dok[0]['dok1'])){
                    unlink('./assets/foto_member/'.$photo->photo);
                    $data['photo']             = $data_dok[0]['dok1'];
                }

                if(isset($data_dok[1]['dok2'])){
                    unlink('./assets/foto_member/'.$photo->file_kemampuan);
                    $data['file_kemampuan']    = $data_dok[1]['dok2'];
                }

                $this->info_member_model->edit_photo($data);
                $this->session->set_flashdata('notif','Photo berhasil di upload');
                
                $this->member_model->update([
                    'full_name' =>  $this->input->post('full_name')
                ],[
                    'id' => mysession('id_user')
                ]);
     

       // print_r($file_k);    
        $this->session->set_userdata([
            "full_name"     =>   $this->input->post('full_name'),
            "full_name_front"=>   $this->input->post('full_name'),
            "photo"         => isset($data_dok[0][' dok1']) ? $data_dok[0]['dok1'] : $photo->photo,
            "is_complete"   => true,
        ]);

       force_redirect('/home/list');
    }

    private function __flash_error($message_error){
        $this->session->set_flashdata(
            "__error",
            $message_error
        );
        force_redirect('/info_member');
    }

   function upload_foto(){
        $config['upload_path']          = './assets/foto_member';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name'] = sha1(time().rand(999,99)).'-'.$_FILES["photo"]['name'];
        $this->load->library('upload', $config);
        $this->upload->do_upload('photo');
       return $this->upload->data();
       

    }

     function upload_file(){
       
        $config['upload_path']          = './assets/data_lamar';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name'] = sha1(time().rand(999,99)).'-'.$_FILES["photo"]['name'];
        $this->load->library('upload', $config);
        $this->upload->do_upload('file_kemampuan');
       return $this->upload->data();
        
    }




    public function form_validate(){
        $this->__validate("full_name",bahasa("full_name"),"required");
        $this->__validate("jenis_kelamin",bahasa("gender"),"required|numeric");
        $this->__validate("no_ktp",bahasa("no_ktp"),"required|numeric");
        $this->__validate("tempat_lahir",bahasa("born"),"required");
        $this->__validate("tanggal_lahir",bahasa("born_date"),"required");
        $this->__validate("umur",bahasa("age"),"required|numeric");
        $this->__validate("status",bahasa("status"),"required|numeric");
        $this->__validate("agama",bahasa("religion"),"required");
        
        // new
        $this->__validate("pendidikan", bahasa("Pendidikan"),"required");
        $this->__validate("tahun_lulus", bahasa("Tahun Lulus"), "required");
        $this->__validate("jurusan", bahasa("Jurusan"), "required");

        $this->__validate("warga_negara",bahasa("citizen"),"required");
        $this->__validate("alamat_tetap",bahasa("address_1"),"required");
        $this->__validate("gol_darah",bahasa("blood_group"),"required");
        $this->__validate("tinggi_badan",bahasa("hight_body"),"required|numeric");
        $this->__validate("berat_badan",bahasa("weight_body"),"required|numeric");
    }

    private function __validate($k, $n, $r){
        $this->form_validation->set_rules($k,$n,$r);
    }

}
