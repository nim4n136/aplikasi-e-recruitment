<?php
Class Login extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model('member_model');
        $this->load->model('info_member_model');
        $this->load->model('data_member_model');
        

    }

    public function action(){
        if($this->session->userdata('data_captcha') != $this->input->post('captcha'))
            $this->__flash_error(bahasa('sorry_captcha_is_wrong'));
        
        $this->__rules_login();
        if($this->form_validation->run() == false)
            $this->__flash_error(validation_errors());

        $get_data_user = $this->member_model->select(['email' => $this->input->post('email')]);
        if(!$get_data_user)
            $this->__flash_error(bahasa('email_not_found'));

        if($get_data_user->verify_email != '1')
            $this->__flash_error(bahasa('email_not_yet_confirm'));

        //if(!password_verify( $this->__post('password'), $get_data_user->password ))
          //  $this->__flash_error(bahasa('wrong_password'));

        $select_info_member =  $this->info_member_model->is_exists(["id_member" => $get_data_user->id]);
        $is_lamar = $this->data_member_model->is_exists_data(
            ['id_member' => $get_data_user->id]
        );
        $this->session->set_userdata(
            [
                "email_front"         => $get_data_user->email,
                "full_name_front"     => $get_data_user->full_name,
                "photo"         => get_info_user('photo',$get_data_user->id),
                "id_user"       => $get_data_user->id,
                "is_complete"   => $select_info_member,
                "is_lamar"      => $is_lamar
            ]
        );
        if(!$select_info_member)
            force_redirect('info_member');
        redirect("home/list");
    }

    private function __post($name){  
        return $this->input->post($name);
    }

    private function __flash_error($message_error){
        $this->session->set_flashdata(
            "error_login",
            $message_error
        );
        $this->__redirect_login();
    }

    private function __redirect_login(){
        redirect('login');exit;
    }

    private function __rules_login(){
        $this->__validate('captcha', 'Captcha', 'required');
        $this->__validate('email', bahasa('email_address'), 'trim|required|valid_email');
        $this->__validate('password', bahasa('password'), 'required');
    }


    private function __validate($k,$n,$r){
        $this->form_validation->set_rules($k,$n,$r);
    }


}
 