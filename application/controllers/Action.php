<?php
Class Action extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model('member_model');
    }
    private $data_member = [];
    function register(){
       $this->__rules();
       if($this->form_validation->run() != false)
            if($this->session->userdata('data_captcha') == $this->input->post('captcha')){
                $email_exists = $this->member_model->check_email(
                    $this->input->post('email')
                );
                if($email_exists){
                    $this->session->set_flashdata(
                        "error_register",
                        bahasa('email_address_already_use')
                    );
                }else
                    $this->save_member();
            }else{
                $this->session->set_flashdata(
                    "error_register",
                    bahasa('sorry_captcha_is_wrong')
                );   
            }
        else{
            $this->session->set_flashdata(
                "error_register",
                validation_errors()
            );
        }
        redirect('register');

    }

    private function save_member(){
        $this->data_member = $data_member = [
            'full_name'     => $this->input->post('full_name'),
            'email'         => $this->input->post('email'),
            'no_handphone'  => $this->input->post('no_hp'),
            'password'      => create_password(rand(999,999)),
            'kode_verify'   => sha1(time().rand(999,999)),
        ];
        $this->send_email_activation();
        $this->member_model->save_member($data_member);
        return $this->session->set_flashdata(
            "success_register",
            bahasa('check_inbox_email_to_activate_the_account')
        );
    }

    private function send_email_activation(){
      $this->data_member['link'] = site_url('action/active/'.base64_encode($this->data_member['kode_verify']));
      $message = $this->load->view('front/email_activation',$this->data_member,TRUE);
      send_email($this->data_member['email'],"Activasi account E-Recruitment", $message);
    }

    public function active($key){
        $decode_key = base64_decode($key);
        print_r($decode_key);
    }

    // bahasa setting
    function __rules(){
        $this->form_validation->set_rules('email', 'Alamat Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('no_hp', 'Nomor Handphone', 'required|numeric');
        $this->form_validation->set_rules('full_name', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'required');
    }

}
