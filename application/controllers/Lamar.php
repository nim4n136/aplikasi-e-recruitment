<?php
Class Lamar extends CI_Controller{
    
    private $data_upload;
    
    function __construct(){
        parent::__construct();
        $this->load->model('jobs_model_member');
        $this->load->model('data_member_model');
        $this->data_upload = list_doc();
    }


    function _remap($param) {
        if(!is_numeric($param)){
            if(method_exists($this,$param))
                $this->$param();
            else
                return view_template('404');
        }else
            $this->index($param);
    }


    private function upload_file($name){
        $config['upload_path']          = './assets/data_lamar';
        $config['allowed_types']        = $this->data_upload[$name]['ext'];
        if(isset($this->data_upload[$name]['size']))
            $config['max_size']      = $this->data_upload[$name]['size']; // kbytes
        $array = explode('.', $_FILES[$name]['name']);
        $extension = end($array);
        $config['file_name'] = $name.'-'.strtolower(str_replace(' ','-',mysession('full_name_front'))).'-'.sha1(time().rand(999,99)).'.'.$extension;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $this->upload->do_upload($name);
        $data = $this->upload->data();
        if($this->upload->display_errors())
            $data['error'] = $this->upload->display_errors();
        $data['file_name'] = $config['file_name'];
        return $data;
    }

    private function __flash_error($message_error, $to = false){
        $this->session->set_flashdata(
            "__error",
            $message_error
        );
        if(!$to)
            $to = $_SERVER['HTTP_REFERER'];
        force_redirect($to);
    }

    function files_to_post(){
        foreach($_FILES as $key => $value){
           $_POST[$key] = $value['name'];
        }
    }

    function save(){
        if($this->data_member_model->is_exists_data(['id_member' => mysession('id_user')]))
            $this->__flash_error(bahasa("Anda telah mengirimkan data pelamar "), '/lamar/status');
        $this->files_to_post();
        $query = $this->jobs_model_member->get_by_id($this->input->post('id'));
        $list_document = back_to_array_list_doc($query->row()->doc);
        foreach($list_document as $doc){
            $get_document_list = $this->data_upload[$doc];
            if($get_document_list['required'])
                $this->__validate($doc,bahasa($get_document_list['name']),"required");
        }
        if($this->form_validation->run() == false)
            $this->__flash_error(validation_errors());

        foreach(array_reverse($_FILES) as $key => $file){
            if(!array_key_exists($key, $this->data_upload))
                continue;
            $upload = $this->upload_file($key);
            if(isset($upload['error'])){
                $this->__flash_error($upload['error']);
                break;
            }
            $to_upload[$key] =  $upload['file_name'];
        }

        // save to  data member
        $this->data_member_model->save_data_member([
            'id_member' => mysession('id_user'),
            'id_jobs'   =>  $this->input->post('id'),
            'status'    => 'pendding'
        ]);

        foreach($to_upload as $name => $value)
            $this->data_member_model->save_data([
                'id_member' =>  mysession('id_user'),
                'name'      =>  $name,
                'value'     =>  $value
            ]);
        
        $this->session->set_flashdata(
            "__error",
            bahasa('Data lamaran sudah di kirim')
        );
        set_session('is_lamar', true);
        force_redirect('/lamar/status');
    }

    private function __validate($k, $n, $r){
        $this->form_validation->set_rules($k,$n,$r);
    }

    function status(){
        if(!mysession('is_lamar'))
            return view_template('404');
        $row = $this->data_member_model->all_data_by_id_member(mysession('id_user'));
        view_template('status',$row);
    }


    function all_status(){
        is_login_member();
        $row = $this->data_member_model->all_data_by_status('accepted_2');
        view_template('all_status',['all_data' => $row]);

    }

    function print(){
        if(!mysession('id_user')) force_redirect('login');
        is_login_member();
        $row = $this->data_member_model->all_data_by_id_member(mysession('id_user'));
        if(!$row)
            view_template('404');
        $this->load->view('front/psikotes_print',$row);

    }

    function index($id_jobs){
        if(!mysession('id_user')) force_redirect('login');
        is_login_member();
        is_lamar();

        $query = $this->jobs_model_member->get_by_id($id_jobs);
            // exit($this->db->last_query());
        $prf = $this->db->get_where('tbl_info_member',array('id_member'=>$this->session->id_user))->row_array();
        $check = $query->num_rows();
        // $row = $prf->row_array();

        if($check > 0){
            $data = [
                'id' => $id_jobs,
                'row' => $query->row(),
                'foto'=>$prf['photo'],
                'action' => site_url('lamar/save')
            ];
            view_template('lamar', $data);
        }else
            view_template('404');

    }
}
