<?php
Class Home extends CI_Controller{
    
    private $data = [];


    function change_lang(){
        $lang_get = $this->input->get('lang',true);
        if($lang_get){
            $lang_get = ($lang_get == "en") ? "english" :"indonesia";
            $this->session->set_userdata('site_lang', $lang_get);
        }
        force_redirect($_SERVER['HTTP_REFERER']);
    }
    function __construct(){
        parent::__construct();
        $this->load->model('jobs_model_member');
        $this->load->model('slider_model');
        $this->load->library('pagination');
        $lang = $this->session->userdata('site_lang');
        $lang = ($lang == "english") ? "en" : "id";
        $this->data = [
            'action_register' => site_url('register/action'),
            'action_login'    => site_url('login/action'),
            'slider'          => $this->slider_model->get_slide_by_lang($lang),
            'captcha'         => load_captcha()
        ];
    }

    function index(){
        is_lamar();

        $this->template->set('title', bahasa('Selamat datang E-Recruitment')." PT.SBS");
        $this->data['jobs'] = $this->jobs_model_member->get_all();
        view_template('home',$this->data);
    }

    function login(){
        is_login_member(true);
        view_template('login', $this->data);
    }
    function register(){
        is_login_member(true);
        view_template('register',$this->data);
    }
   
    function list(){
        is_login_member();
        is_lamar();
        $search_term = ''; // default when no term in session or POST
        $po = $this->input->post();
        if (isset($po['q'])){
            $search_term = $this->input->post('q');
            $this->session->set_userdata('q', $search_term);
        }elseif ($this->session->userdata('q'))
            $search_term = $this->session->userdata('q');
        $config['base_url'] = site_url('home/list'); //site url
        $config['total_rows'] = $this->jobs_model_member->count_all($search_term); 
        $config['per_page'] = 5;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
 
      $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['data'] = $this->jobs_model_member->get_list($config["per_page"], $data['page'], $search_term);           
        $data['pagination'] = $this->pagination->create_links();
        $data['q'] = $search_term;
        view_template('list',$data);
    }


    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }

    function organitation_structure(){
        $x['cf']=  $this->db->get('config_rn',array('id'=>'1'));
        view_template('halaman_detail',$x);
    }

    function detail($id){
        
        is_lamar();
        $get = $this->jobs_model_member->get_by_id($id);
        $row = $get->row();
        view_template('detail',['data_job' => $row]);
    }
}
