<?php
Class Register extends CI_Controller{
    
    private $data_member = [];

    function __construct(){
        parent::__construct();
        $this->load->model('member_model');
    }

    function action(){
       $this->__rules();
       if($this->form_validation->run() != false)
            if($this->session->userdata('data_captcha') == $this->input->post('captcha')){
                $email_exists = $this->member_model->check_email(
                    $this->input->post('email')
                );
                if($email_exists){
                    $this->session->set_flashdata(
                        "error_register",
                        bahasa('email_address_already_use')
                    );
                }else
                    $this->save_member();
            }else{
                $this->session->set_flashdata(
                    "error_register",
                    bahasa('sorry_captcha_is_wrong')
                );   
            }
        else{
            $this->session->set_flashdata(
                "error_register",
                validation_errors()
            );
        }
        redirect('register');

    }

    private function save_member(){
        $this->data_member = $data_member = [
            'full_name'     => $this->input->post('full_name'),
            'email'         => $this->input->post('email'),
            'no_handphone'  => $this->input->post('no_hp'),
            'password'      => create_password(rand(999,999)),
            'kode_verify'   => sha1(time().rand(999,999)),
        ];
        $this->send_email_activation();
        $this->member_model->save_member($data_member);
        return $this->session->set_flashdata(
            "success_register",
            bahasa('check_inbox_email_to_activate_the_account')
        );
    }

    private function send_email_activation(){
      $this->data_member['link'] = site_url('register/active/'.base64_encode($this->data_member['kode_verify']));
      $message = $this->load->view('front/email_activation',$this->data_member,TRUE);
      send_email($this->data_member['email'],"Activasi account E-Recruitment", $message);
    }

    public function active($key){
        $decode_key = base64_decode($key);
        $data = [
            'exists' => $this->member_model->is_exists(
                [
                    'kode_verify' => $decode_key,
                    'verify_email' => "0"
                ]
            )
        ];
        $data['error'] = false;
        if($this->input->method() == "post"){
            $created = $this->create_new_password( $decode_key);
            if($created){
                $this->session->set_flashdata(
                    "confirm_email_success",
                    bahasa('confirm_email_success_please_login')
                );
                redirect('login');exit;
            }else
                $data['error'] = bahasa('failed_to_created_new_password');
        }
        if(validation_errors())
            $data['error'] = validation_errors();

        view_template('active',$data);
    }

    private function create_new_password($kode_ver){
        $this->form_validation->set_rules('password', bahasa('password'), 'required|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('confirm_password', bahasa('confirm_password'), 'required|matches[password]');
        if($this->form_validation->run() != false){
            return $this->member_model->update(
                [
                    'password' => create_password($this->input->post('password')),
                    'verify_email' => 1,
                    'kode_verify'  => sha1(time().rand(999,9999))
                ],
                [
                    'kode_verify' => $kode_ver
                ]
            );
        }
    }

    // bahasa setting
    function __rules(){
        $this->form_validation->set_rules('email', bahasa('email_address'), 'trim|required|valid_email');
        $this->form_validation->set_rules('no_hp', bahasa('no_hp'), 'required|numeric');
        $this->form_validation->set_rules('full_name', bahasa('full_name'), 'required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'required');
    }


    // function mail(){
    //     mail('isafesbs@gmail.com', 'ts', 'ts');
    // }

}
