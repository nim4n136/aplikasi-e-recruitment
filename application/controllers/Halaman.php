<?php 

class Halaman extends CI_controller{

	function __construct(){
		parent::__construct();

	}
 
	function managable($action='',$id=''){

		if ($id) {  
			$db =$this->db->get_where('config_rn',array('id'=>$id));
			if ($db->num_rows() > 0) {
				$x['judul_j']=$db->row()->judul;
				$x['isi']=$db->row()->isi;
				$x['tanggal']=$db->row()->tanggal;
				$x['id_user']=$db->row()->id_user;
			}else{
				redirect(base_url('index.php/admin/halaman/managable'));
			}	 
		}else{
			$x['judul_j']='';
			$x['isi']='';
			$x['tanggal']='';
			$x['id_user']='';

		}
      if($action =='add'){
			if(isset($_POST['kirim'])){
			$data=[ 
					'judul'=>$this->input->post('judul'),
					'isi'=>$this->input->post('isi'),
					'tanggal'=>date('Y-m-d'),
					'id_user'=>$this->session->userdata('id_users')
				];
				$cek =$this->db->insert('config_rn',$data);
				if ($cek) {
					$this->session->set_flashdata('pesan','<div class="callout callout-info">Insert Successfuly</div>');
					redirect(base_url('index.php/admin/halaman/managable'));	  
				}else{
					$this->session->set_flashdata('pesan','<div class="callout callout-danger">Insert Faild</div>');
					redirect(base_url('index.php/admin/halaman/managable'));	 
				}
			}else{
				$x['judul'] ='Data Halaamn';
				$x['form'] ='y';
				$this->template->load('template','admin/halaman',$x);
			}
		}elseif($action =='edit'){
            			if(isset($_POST['kirim'])){
				$data=[ 
					'judul'=>$this->input->post('judul'),
					'isi'=>$this->input->post('isi'),
					'tanggal'=>date('Y-m-d'),
					'id_user'=>$this->session->userdata('id_users'),
				];

				$db =$this->db->update('config_rn',$data,array('id'=>$id));
				if ($cek) {
					$this->session->set_flashdata('pesan','<div class="callout callout-info">Insert Successfuly</div>');
					redirect(base_url('index.php/Halaman/managable'));	  
				}else{
					$this->session->set_flashdata('pesan','<div class="callout callout-danger">Insert Faild</div>');
					redirect(base_url('index.php/Halaman/managable'));	 
				}
			}else{
				$x['judul'] ='Data Halaamn';
				$x['form'] ='y';
				$this->template->load('template','admin/halaman',$x);
			}
		}elseif($action =='delete'){
			if ($id ='1') {
				# code...
				$this->session->set_flashdata('pesan','<div class="callout callout-danger">Cannot Delete .</div>');
					redirect(base_url('index.php/halaman/managable'));	
			}else{ 
			$cek =$this->db->delete('config_rn',array('id'=>$id));
				if ($cek) {
					$this->session->set_flashdata('pesan','<div class="callout callout-info">Delete Successfuly</div>');
					redirect(base_url('index.php/halaman/managable'));	  
				}else{
					$this->session->set_flashdata('pesan','<div class="callout callout-danger">Insert Faild</div>');
					redirect(base_url('index.php/halaman/managable'));	 
				}
			}
		}else{
          $x['judul'] ='Data Halaamn';
          $x['form'] ='n';
          $x['data'] =$this->db->get('config_rn');
				$this->template->load('template','admin/halaman',$x);

		}	 


	}






}