<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function index() {
        is_login();
        $data['daftar_jobs'] = $this->db->count_all_results('tbl_list_jobs');
        $data['daftar_pelamar'] = $this->db->count_all_results('tbl_status_member');
        $this->db->where('status','pendding');
        $data['daftar_pelamar_pendding'] = $this->db->count_all_results('tbl_status_member');
        $this->db->where('status','rejected_1');
        $this->db->or_where('status','rejected_2');
        $data['daftar_pelamar_close'] = $this->db->count_all_results('tbl_status_member');
        $this->template->load('template', 'dashboard',$data);
    }
}
