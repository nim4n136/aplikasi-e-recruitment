<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Controller
{
    public function index(){
        $this->template->load('template','admin/settings', [
            "error" => $this->session->flashdata('_error'),
            "msg"   => $this->session->flashdata('_msg'),
        ]);
    }

    public function save(){
        $inputInterface = [
            "address",
            "about",
            "new",
            "email",
            "brand",
            "phone",
            "facebook",
            "youtube",
            "twitter",
            "lang_default"
            
        ];

        $input = $this->input->post();
        foreach($input as $key => $value)
            if(!in_array($key, $inputInterface))
                $this->flash("Ada data yang tida di izinkan" ,true);
        $lang = $this->input->post('lang_default');
        if(!in_array($lang,['id','en']))
            $this->flash("Masukan bahasa dengan benar", true);
        foreach($this->input->post() as $key => $value)
            $this->update($key, $value);
        $this->flash("Update data berhasil !!!");
    }

    public function update($name, $value){
        $this->db->set('value', $value);
        $this->db->where('nama_setting', $name);
        $this->db->update('tbl_settings');
    }

    public function flash($message, $error = false){
        $this->session->set_flashdata("_msg" , $message);
        $this->session->set_flashdata("_error" , $error);
        force_redirect(site_url('admin/settings'));
    }

}