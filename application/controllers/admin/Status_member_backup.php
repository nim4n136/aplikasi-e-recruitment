<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Status_member extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Status_member_model');
        $this->load->model('Data_member_model');
        $this->load->library('form_validation');        
    	$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template','admin/status_member/tbl_status_member_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Status_member_model->json();
    }

    public function read($id) 
    {
        $row = $this->Data_member_model->all_data_by_id_member($id);
        if ($row) {
            $row['data_doc'] =  $this->Data_member_model->get_data_by_id_member($id);
            $this->template->load('template','admin/status_member/tbl_status_member_read', $row);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/status_member'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/status_member/create_action'),
	    'id' => set_value('id'),
	    'id_member' => set_value('id_member'),
	    'id_jobs' => set_value('id_jobs'),
	    'status' => set_value('status'),
	);
        $this->template->load('template','admin/status_member/tbl_status_member_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_member' => $this->input->post('id_member',TRUE),
		'id_jobs' => $this->input->post('id_jobs',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Status_member_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('admin/status_member'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Status_member_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/status_member/update_action'),
		'id' => set_value('id', $row->id),
		'id_member' => set_value('id_member', $row->id_member),
		'id_jobs' => set_value('id_jobs', $row->id_jobs),
		'status' => set_value('status', $row->status),
	    );
            $this->template->load('template','admin/status_member/tbl_status_member_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/status_member'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'id_member' => $this->input->post('id_member',TRUE),
		'id_jobs' => $this->input->post('id_jobs',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Status_member_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/status_member'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Data_member_model->is_exists_data(['id_member' => $id]);
        if ($row) {
            $document = $this->Data_member_model->get_data_by_id_member($id);
            $this->Data_member_model->delete($id);
            foreach($document as $row){
                unlink('assets/data_lamar/'.$row['value']);
            }
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/status_member'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/status_member'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_member', 'id member', 'trim|required');
	$this->form_validation->set_rules('id_jobs', 'id jobs', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_status_member.xls";
        $judul = "tbl_status_member";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Member");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Jobs");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Status_member_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_member);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_jobs);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=tbl_status_member.doc");

        $data = array(
            'tbl_status_member_data' => $this->Status_member_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('admin/status_member/tbl_status_member_doc',$data);
    }

}

/* End of file Admin/Status_member.php */
/* Location: ./application/controllers/Admin/Status_member.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-11-03 21:20:48 */
/* http://harviacode.com */