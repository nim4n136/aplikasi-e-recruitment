<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Status_member extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Status_member_model');
        $this->load->model('Data_member_model');
        $this->load->library('form_validation');        
    	$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template','admin/status_member/tbl_status_member_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Status_member_model->json();
    }

    public function read($id) 
    {
        $row = $this->Data_member_model->all_data_by_id_member($id);
        if ($row) {
            $row['data_doc'] =  $this->Data_member_model->get_data_by_id_member($id);
            $row['action_update_data']   = site_url('admin/status_member/update_action/'.$id);
            
            $this->template->load('template','admin/status_member/tbl_status_member_read', $row);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/status_member'));
        }
    }

    public function upload_now(){
        if(in_array($this->input->post('status', TRUE), ['accepted_2','rejected_2'])){
            $upload = $this->upload_file('file_psikotes');
            if(isset($upload['error'])){
                $this->session->set_flashdata('__error', $upload['error']);
                redirect($_SERVER['HTTP_REFERER']);exit;
            }
            return $upload;
        }
    }
    
    public function update_action($id) 
    {
        
        if(@$_FILES['file_psikotes']['name'] != false)
            $upload = $this->upload_now();
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        $json = base64_decode($this->input->post('json'));
        $json = json_decode($json);

        if(@$json->file_name == NULL)
            $upload = $this->upload_now();
        else
            $upload['file_name'] = $json->file_name;
      
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('__error', 'Failed, data status tidak di temukan');
        }else{
            $data = json_encode(['file_name' => $upload['file_name'], "keterangan" => $this->input->post('keterangan', TRUE)]);
            $this->Data_member_model->update_status(['status' => $this->input->post('status', TRUE),'file_psikotes' => $data] , $id);
            $this->session->set_flashdata('__success', 'Success, Update status berhasil');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }



    private function upload_file($name){
        $config['upload_path']          = './assets/data_lamar';
        $config['allowed_types']        = 'xls|xlsx|xlsx|xlsm';
        $array = explode('.', $_FILES[$name]['name']);
        $extension = end($array);
        $config['file_name'] = $name.'-'.sha1(time().rand(999,99)).'.'.$extension;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $this->upload->do_upload($name);
        $data = $this->upload->data();
        if($this->upload->display_errors())
            $data['error'] = $this->upload->display_errors();
        $data['file_name'] = $config['file_name'];
        return $data;
    }


    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "data_pelamar.xls";
        $judul = "Data Pelamar";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        // penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Posisi");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Lengkap");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Kelamin");
	xlsWriteLabel($tablehead, $kolomhead++, "No KTP");
	xlsWriteLabel($tablehead, $kolomhead++, "Umur");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Tempat tanggal lahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Warga negara");
	xlsWriteLabel($tablehead, $kolomhead++, "Agama");
	xlsWriteLabel($tablehead, $kolomhead++, "Golongan darah");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat tetap");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat sekarang");
	xlsWriteLabel($tablehead, $kolomhead++, "Tinggi badan");
	xlsWriteLabel($tablehead, $kolomhead++, "Berat badan");
	xlsWriteLabel($tablehead, $kolomhead++, "Pendidikan");
	xlsWriteLabel($tablehead, $kolomhead++, "Tahun lulus");
	xlsWriteLabel($tablehead, $kolomhead++, "Jurusan");
	xlsWriteLabel($tablehead, $kolomhead++, "Email");
	xlsWriteLabel($tablehead, $kolomhead++, "No HP");
    xlsWriteLabel($tablehead, $kolomhead++, "Diterima /Di tolak");
     
        foreach ($this->Data_member_model->get_all() as $data) {

        //    enum('accepted_1','pendding','rejected_1','accepted_2','rejected_2')

            if ($data['status'] == 'accepted_1' || $data['status'] == 'accepted_2' ) {
                $status = 'Diterima';
            }elseif($data['status'] == 'rejected_1' || $data['status'] =='rejected_2'){ 
                $status = 'Di tolak';
            }elseif($data['status'] == 'pendding' ){ 
                $status = 'Pending';
            }else{
                $status = 'Nothing ';
            } 



            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data['name']);
        xlsWriteLabel($tablebody, $kolombody++, $data['full_name']);
        xlsWriteLabel($tablebody, $kolombody++, jenis_kelamin($data['jenis_kelamin']));
        xlsWriteLabel($tablebody, $kolombody++, $data['no_ktp']);
        xlsWriteLabel($tablebody, $kolombody++, $data['umur']);
        xlsWriteLabel($tablebody, $kolombody++, ($data['status']== 1) ? "Menikah" : "Belum Menikah");
        xlsWriteLabel($tablebody, $kolombody++, $data['tempat_lahir'].", ".$data['tanggal_lahir']);
        xlsWriteLabel($tablebody, $kolombody++, $data['warga_negara']);
        xlsWriteLabel($tablebody, $kolombody++, $data['agama']);
        xlsWriteLabel($tablebody, $kolombody++, $data['gol_darah']);
        xlsWriteLabel($tablebody, $kolombody++, $data['alamat_tetap']);
        xlsWriteLabel($tablebody, $kolombody++, $data['alamat_sekarang']);
        xlsWriteLabel($tablebody, $kolombody++, $data['tinggi_badan']);
        xlsWriteLabel($tablebody, $kolombody++, $data['berat_badan']);
        xlsWriteLabel($tablebody, $kolombody++, $data['pendidikan']);
        xlsWriteLabel($tablebody, $kolombody++, $data['tahun_lulus']);
        xlsWriteLabel($tablebody, $kolombody++, $data['jurusan']);
        xlsWriteLabel($tablebody, $kolombody++, $data['email']);
        xlsWriteLabel($tablebody, $kolombody++, $data['no_handphone']);
         xlsWriteLabel($tablebody, $kolombody++, $status); 

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
    
    public function delete($id) 
    {
        $row = $this->Data_member_model->is_exists_data(['id_member' => $id]);
        if ($row) {
            $document = $this->Data_member_model->get_data_by_id_member($id);
            $this->Data_member_model->delete($id);
            foreach($document as $row){
                unlink('assets/data_lamar/'.$row['value']);
            }
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/status_member'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/status_member'));
        }
    }

}