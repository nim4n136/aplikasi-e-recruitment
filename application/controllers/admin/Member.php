<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Member_model_c');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template','admin/member/tbl_member_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Member_model_c->json();
    }

    public function read($id) 
    {
        $row = $this->Member_model_c->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'full_name' => $row->full_name,
		'email' => $row->email,
		'password' => $row->password,
		'no_handphone' => $row->no_handphone,
		'verify_email' => $row->verify_email,
		'kode_verify' => $row->kode_verify,
	    );
            $this->template->load('template','admin/member/tbl_member_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/member'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/member/create_action'),
	    'id' => set_value('id'),
	    'full_name' => set_value('full_name'),
	    'email' => set_value('email'),
	    'password' => set_value('password'),
	    'no_handphone' => set_value('no_handphone'),
	    'verify_email' => set_value('verify_email'),
	    'kode_verify' => set_value('kode_verify'),
	);
        $this->template->load('template','admin/member/tbl_member_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'full_name' => $this->input->post('full_name',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => create_password($this->input->post('password',TRUE)),
		'no_handphone' => $this->input->post('no_handphone',TRUE),
		'verify_email' => $this->input->post('verify_email',TRUE),
		'kode_verify' => $this->input->post('kode_verify',TRUE),
	    );

            $this->Member_model_c->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('admin/member'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Member_model_c->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/member/update_action'),
		'id' => set_value('id', $row->id),
		'full_name' => set_value('full_name', $row->full_name),
		'email' => set_value('email', $row->email),
		'password' => set_value('password', $row->password),
		'no_handphone' => set_value('no_handphone', $row->no_handphone),
		'verify_email' => set_value('verify_email', $row->verify_email),
		'kode_verify' => set_value('kode_verify', $row->kode_verify),
	    );
            $this->template->load('template','admin/member/tbl_member_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/member'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'full_name' => $this->input->post('full_name',TRUE),
		'email' => $this->input->post('email',TRUE),
		'no_handphone' => $this->input->post('no_handphone',TRUE),
		'verify_email' => $this->input->post('verify_email',TRUE),
		'kode_verify' => $this->input->post('kode_verify',TRUE),
        );
        if($this->input->post('password',TRUE))
            $data['password'] = create_password($this->input->post('password',TRUE));

        $this->Member_model_c->update($this->input->post('id', TRUE), $data);
        $this->session->set_flashdata('message', 'Update Record Success');
        redirect(site_url('admin/member'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Member_model_c->get_by_id($id);

        if ($row) {
            $this->Member_model_c->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/member'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/member'));
        }
    }

    public function _rules() 
    {
        $this->form_validation->set_rules('full_name', 'full name', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('no_handphone', 'no handphone', 'trim|required');
        $this->form_validation->set_rules('verify_email', 'verify email', 'trim|required');
        $this->form_validation->set_rules('kode_verify', 'kode verify', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_member.xls";
        $judul = "tbl_member";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Full Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Email");
	xlsWriteLabel($tablehead, $kolomhead++, "No Handphone");
	xlsWriteLabel($tablehead, $kolomhead++, "Verify Email");

	foreach ($this->Member_model_c->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->full_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->email);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_handphone);
	    xlsWriteLabel($tablebody, $kolombody++, $data->verify_email);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=tbl_member.doc");

        $data = array(
            'tbl_member_data' => $this->Member_model_c->get_all(),
            'start' => 0
        );
        
        $this->load->view('admin/member/tbl_member_doc',$data);
    }

}

/* End of file Admin/Member.php */
/* Location: ./application/controllers/Admin/Member.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-27 06:26:57 */
/* http://harviacode.com */