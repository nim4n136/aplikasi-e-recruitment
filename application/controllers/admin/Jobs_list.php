<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jobs_list extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Jobs_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template','admin/jobs_list/tbl_list_jobs_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Jobs_model->json();
    }

    public function read($id) 
    {
        $row = $this->Jobs_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'date_active' => $row->date_active,
		'date_expire' => $row->date_expire,
		'max_age' => $row->max_age,
		'academic' => $row->academic,
		'experience' => $row->experience,
		'salary' => $row->salary,
		'description' => $row->description,
		'doc' => $row->doc,
	    );
            $this->template->load('template','admin/jobs_list/tbl_list_jobs_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/jobs_list'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/jobs_list/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	    'date_active' => set_value('date_active'),
	    'date_expire' => set_value('date_expire'),
	    'max_age' => set_value('max_age'),
	    'academic' => set_value('academic'),
	    'experience' => set_value('experience'),
	    'salary' => set_value('salary'),
	    'description' => set_value('description'),
	    'doc' => set_value('doc'),
	);
        $this->template->load('template','admin/jobs_list/tbl_list_jobs_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'date_active' => $this->input->post('date_active',TRUE),
		'date_expire' => $this->input->post('date_expire',TRUE),
		'max_age' => $this->input->post('max_age',TRUE),
		'academic' => $this->input->post('academic',TRUE),
		'experience' => $this->input->post('experience',TRUE),
		'salary' => $this->input->post('salary',TRUE),
		'description' => $this->input->post('description',TRUE),
		'doc' => $this->input->post('doc',TRUE),
	    );

        $data['date_expire'] = date_id_en($data['date_expire']);
        $data['date_active'] = date_id_en($data['date_active']);
            $this->Jobs_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('admin/jobs_list'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Jobs_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/jobs_list/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'date_active' => set_value('date_active', $row->date_active),
		'date_expire' => set_value('date_expire', $row->date_expire),
		'max_age' => set_value('max_age', $row->max_age),
		'academic' => set_value('academic', $row->academic),
		'experience' => set_value('experience', $row->experience),
		'salary' => set_value('salary', $row->salary),
		'description' => set_value('description', $row->description),
		'doc' => set_value('doc', $row->doc),
	    );
            $this->template->load('template','admin/jobs_list/tbl_list_jobs_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/jobs_list'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'date_active' => $this->input->post('date_active',TRUE),
		'date_expire' => $this->input->post('date_expire',TRUE),
		'max_age' => $this->input->post('max_age',TRUE),
		'academic' => $this->input->post('academic',TRUE),
		'experience' => $this->input->post('experience',TRUE),
		'salary' => $this->input->post('salary',TRUE),
		'description' => $this->input->post('description',TRUE),
		'doc' => save_to_db_list_doc($this->input->post('doc')),
        );
        
        $data['date_expire'] = date_id_en($data['date_expire']);
        $data['date_active'] = date_id_en($data['date_active']);
            $this->Jobs_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/jobs_list'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Jobs_model->get_by_id($id);

        if ($row) {
            $this->Jobs_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/jobs_list'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/jobs_list'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('date_active', 'date active', 'trim|required');
	$this->form_validation->set_rules('date_expire', 'date expire', 'trim|required');
	$this->form_validation->set_rules('max_age', 'max age', 'trim|required');
	$this->form_validation->set_rules('academic', 'academic', 'trim|required');
	$this->form_validation->set_rules('experience', 'experience', 'trim|required');
	$this->form_validation->set_rules('salary', 'salary', 'trim|required');
	$this->form_validation->set_rules('description', 'description', 'trim|required');
	$this->form_validation->set_rules('doc[]', 'doc', 'required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Admin/jobs_list.php */
/* Location: ./application/controllers/Admin/jobs_list.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-28 23:09:24 */
/* http://harviacode.com */