<?php
Class Auth extends CI_Controller{
    
    
    
    function index(){
        $this->load->view('auth/login');
    }
    
    function cheklogin(){
        $email      = $this->input->post('email');
        //$password   = $this->input->post('password');
        $password = $this->input->post('password',TRUE);
        $hashPass = password_hash($password,PASSWORD_DEFAULT);
        $test     = password_verify($password, $hashPass);
        // query chek users
        $this->db->where('email',$email);
        //$this->db->where('password',  $test);
        $users       = $this->db->get('tbl_user');
        if($users->num_rows()>0){
            $user = $users->row_array();
            if(password_verify($password,$user['password'])){
                $this->load->model('Logs_model');

                $this->Logs_model->create("Melakukan login", $email);
                // retrive user data to session
                 $_SESSION['KCFINDER']              =array();
                 $_SESSION['KCFINDER']['disabled']  = false;
                 $_SESSION['KCFINDER']['uploadURL'] = base_url()."/assets/file/";
                $this->session->set_userdata($user);
                redirect('admin');
            }else{
                redirect('admin/auth');
            }
        }else{
            $this->session->set_flashdata('status_login','email atau password yang anda input salah');
            redirect('admin/auth');
        }
    }
    
    function logout(){
        $this->session->sess_destroy();
        $this->load->model('Logs_model');
        $userdata=  $this->session->all_userdata();
        $this->Logs_model->create("Melakukan logout", 
           $userdata['email']
        );

        $this->session->set_flashdata('status_login','Anda sudah berhasil keluar dari aplikasi');
        redirect('admin/auth');
    }
}
