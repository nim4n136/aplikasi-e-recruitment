<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logs extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Logs_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $logs = $this->Logs_model->get_all();

        $data = array(
            'logs_data' => $logs
        );

        $this->template->load('template','tbl_logs_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Logs_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'message' => $row->message,
		'ip' => $row->ip,
		'time' => $row->time,
	    );
            $this->template->load('template','tbl_logs_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('logs'));
        }
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_logs.xls";
        $judul = "tbl_logs";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
    xlsWriteLabel($tablehead, $kolomhead++, "Email");
	xlsWriteLabel($tablehead, $kolomhead++, "Message");
	xlsWriteLabel($tablehead, $kolomhead++, "Ip");
	xlsWriteLabel($tablehead, $kolomhead++, "Time");

	foreach ($this->Logs_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
        xlsWriteLabel($tablebody, $kolombody++, $data->email);
	    xlsWriteLabel($tablebody, $kolombody++, $data->message);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ip);
	    xlsWriteLabel($tablebody, $kolombody++, $data->time);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}
