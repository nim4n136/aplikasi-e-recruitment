<?php 


   /**
   * 
   */
   class Profil extends CI_controller
   {
    
    function __construct()
    {
      parent::__construct();
      if($this->session->userdata('id_users') ==''){
      	redirect(base_url( 
      		'index.php?lg=forbiden'));
      	exit();
      	
      }
    }

    function index(){
     if (isset($_POST['kirim'])) {
      # code...
      $this->form_validation->set_rules('email','email','required|is_unique[email.tbl_user]');
      $this->form_validation->set_rules('email','password','required');
      
      if($this->form_validation->run() == TRUE){ 
       $cf['file_name'] = 'pf'.time();
       $cf['allowed_types'] = 'jpg|png';
       $cf['upload_path']  ="./assets/foto_profil";
      
       $this->upload->initialize($cf);
       if($this->upload->do_upload('gambar') == TRUE){ 
        $password = $this->input->post('password',TRUE);
        $hashPass = password_hash($password,PASSWORD_DEFAULT);

        $update= [  
        'full_name'=>$this->input->post('full_name'),
        'email'=>$this->input->post('email'),
        'password'=>$hashPass,
        'images'=>$this->upload->file_name, 
      ];

  $foto= $this->db->get_where('tbl_user',array('id_users'=>$this->session->userdata('id_users')))->row_array();
      @unlink('./assets/foto_profil/'.$foto['images']);
     $cek= $this->db->update('tbl_user',$update,array('id_users'=>$this->session->userdata('id_users')));
     if($cek){
           $this->session->set_flashdata('pesan','<div class="callout callout-success">My username  hasbeen changed. password is : '.$this->input->post('password').'</div>');
      redirect(base_url('index.php/admin/profil'));  
     }else{
           
        $this->session->set_flashdata('pesan','<div class="callout callout-danger">data base errror or please contact administrator.</div>');
      redirect(base_url('index.php/admin/profil')); 

     }
  }else{
       $this->session->set_flashdata('pesan',$this->upload->display_errors('<div class="callout callout-danger">','</div>'));
      redirect(base_url('index.php/admin/profil')); 
  } 
    }else{
      $this->session->set_flashdata('pesan',validation_errors('<div class="callout callout-danger">','</div>'));
      redirect(base_url('index.php/admin/profil'));
    }
  }else{
    $x['title'] = "Edit Password Login";
    $x['data'] =$this->db->get_where('tbl_user',array('id_users'=>$this->session->userdata('id_users')))->row_array();
    $this->template->load('template','profil',$x);
  }
 
}
}