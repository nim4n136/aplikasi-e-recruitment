<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Slider extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('slider_model');
        $this->load->library('form_validation');        
	    $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template','admin/slider/tbl_slider_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->slider_model->json();
    }

    public function read($id) 
    {
        $row = $this->slider_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'pic' => $row->pic,
                'title' => $row->title,
                'description' => $row->description,
                'lang' => $row->lang
            );

            $this->template->load('template','admin/slider/tbl_slider_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/slider'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/slider/create_action'),
            'id' => set_value('id'),
            'pic' => set_value('pic'),
            'title' => set_value('title'),
            'description' => set_value('description'),
            'lang' => set_value('lang'),
        );
        

        $this->template->load('template','admin/slider/tbl_slider_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'title' => $this->input->post('title',TRUE),
                'description' => $this->input->post('description',TRUE),
                'lang' => $this->input->post('lang',TRUE),
            );
            $upload = $this->upload_foto();
            if($upload['file_name']) $data['pic'] = $upload['file_name'];

            $this->slider_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('admin/slider'));
        }
    }

    function upload_foto(){
        $config['upload_path']          = './assets/foto_slide';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name'] = sha1(time().rand(999,99)).'-'.$_FILES["pic"]['name'];
        $this->load->library('upload', $config);
        $this->upload->do_upload('pic');
        return $this->upload->data();
    }
    
    public function update($id) 
    {
        $row = $this->slider_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/slider/update_action'),
                'id' => set_value('id', $row->id),
                'pic' => set_value('pic', $row->pic),
                'title' => set_value('title', $row->title),
                'description' => set_value('description', $row->description),
                'lang' => set_value('description', $row->lang)
            );
            $this->template->load('template','admin/slider/tbl_slider_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/slider'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            
            $data = array(
                'title' => $this->input->post('title',TRUE),
                'description' => $this->input->post('description',TRUE),
                'description' => $this->input->post('lang',TRUE),
            );

            if($_FILES["pic"]['name']){
                $upload = $this->upload_foto();
                if($upload['file_name']) $data['pic'] = $upload['file_name'];
            }

            $this->slider_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/slider'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->slider_model->get_by_id($id);
        if ($row) {
            $this->slider_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/slider'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/slider'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('description', 'description', 'trim|required');
	$this->form_validation->set_rules('lang', 'lang', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}



/* End of file Admin/Slider.php */
/* Location: ./application/controllers/Admin/Slider.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-31 17:10:59 */
/* http://harviacode.com */