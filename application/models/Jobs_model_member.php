<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jobs_model_member extends CI_Model
{

    public $table = 'tbl_list_jobs';

    public function get_all(){
        $this->db->where("date_expire > NOW() AND date_active < NOW() ");
        $query = $this->db->get($this->table);
        return $query;
    }
    public function get_list($limit, $start, $q){
        if($q)
            $this->search($q);
        $this->db->where("date_expire > NOW() AND date_active < NOW() ");
        return $this->db->get($this->table, $limit, $start);
    }
    private function search($q){
	$this->db->group_start();
        $this->db->or_like([
            'name' => $q,
            'max_age' => $q,
            'academic' => $q,
            'experience' => $q,
            'description' => $q
        ]);
	$this->db->group_end();

    }

    public function count_all($q){
        $this->search($q);
	$this->db->where("date_expire > NOW() AND date_active < NOW() ");
        return $this->db->count_all_results($this->table);
    }
    public function get_by_id($id){    
        // $this->db->where("date_expire > NOW() AND date_active < NOW() ");
        $this->db->where("id", $id);
        return $this->db->get($this->table);
    }
}
