<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Info_member_model extends CI_Model
{
    public $table = "tbl_info_member";

    public function is_exists($where){
        $this->db->select('id');
        $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }
    
    public function select($where, $select = "*"){
        $this->db->select($select);
        $this->db->where($where);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    public function select_file()
    {
        
        $this->db->from('tbl_info_member');
        $this->db->where('id',mysession('id_user'));
        $query = $this->db->get();
        return $query->row();
    }

    public function edit_photo($data)
    {
        $this->db->where('id_member',$data['id']);
        $this->db->update('tbl_info_member',$data);
    }
}