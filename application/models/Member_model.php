<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_model extends CI_Model
{
    public $table = "tbl_member";

    public function check_email($email){
        $this->db->select('id');
        $this->db->where('email', $email);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    public function is_exists($where){
        $this->db->select('id');
        $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }
    
    public function select($where, $select = "*"){
        $this->db->select($select);
        $this->db->where($where);
        return $this->db->get($this->table)->row();
    }

    public function update($data, $data_where){
        $this->db->where($data_where);
        return $this->db->update($this->table, $data);
    }

    public function save_member($data){
        return $this->db->insert($this->table,$data);
    }
}