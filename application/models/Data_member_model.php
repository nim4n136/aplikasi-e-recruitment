<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Data_member_model extends CI_Model
{
    public $table_status_member = "tbl_status_member";
    public $table_data = "tbl_data";
    public $table_member = "tbl_member";
    public $table_info_member = "tbl_info_member";
    public $table_list_jobs = "tbl_list_jobs";

    public function is_exists_data($where){
        $this->db->select('id');
        $this->db->where($where);
        $query = $this->db->get($this->table_data);
        return $query->num_rows();
    }

    public function update_status($value,$id_member){
        $this->db->where('id_member', $id_member);
        return $this->db->update($this->table_status_member,  $value);
    }

    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table_member);
        $this->db->where('id_member', $id);
        $this->db->delete($this->table_data);
        $this->db->where('id_member', $id);
        $this->db->delete($this->table_status_member);
        $this->db->where('id_member', $id);
        $this->db->delete($this->table_info_member);
    }

    public function get_data_by_id_member($id){
        $this->db->select('*');
        $this->db->where("id_member", $id);
        $query = $this->db->get($this->table_data);
        return $query->result_array();
    }

    public function all_data(){
        $this->db->select(
            "a.full_name, a.email, a.no_handphone, b.photo,b.no_ktp,b.motivasi,b.file_kemampuan,  b.tempat_lahir,  b.tanggal_lahir,  b.umur,  b.jenis_kelamin,  b.status,  b.pendidikan,  b.tahun_lulus,  b.jurusan,  b.warga_negara,  b.agama,  b.gol_darah,  b.alamat_tetap,  b.alamat_sekarang,  b.tinggi_badan,  b.berat_badan, d.name, d.max_age, d.academic, d.experience, d.salary, c.status , c.file_psikotes"
        );

        $this->db->from("{$this->table_member} a"); 
        $this->db->join(
            "{$this->table_info_member} b",
            "b.id_member = a.id"
        );

        $this->db->join(
            "{$this->table_status_member} c",
            "c.id_member = a.id"
        );

        $this->db->join(
            "{$this->table_list_jobs} d",
            "d.id = c.id_jobs"
        );
    }

    public function all_data_by_status($status){
        $this->all_data();
        $this->db->where("c.status",$status);
        return $query = $this->db->get()->result(); 
    }

    public function all_data_by_id_member($id){
        $this->all_data();
        $this->db->where("a.id",$id);
        return $query = $this->db->get()->row_array(); 
    }
    
    public function get_all(){
        $this->db->select(
            "a.full_name, a.email, a.no_handphone, b.photo,b.no_ktp,  b.tempat_lahir,  b.tanggal_lahir,  b.umur,  b.jenis_kelamin,  b.status,  b.pendidikan,  b.tahun_lulus,  b.jurusan,  b.warga_negara,  b.agama,  b.gol_darah,  b.alamat_tetap,  b.alamat_sekarang,  b.tinggi_badan,  b.berat_badan, d.name, d.max_age, d.academic, d.experience, d.salary, c.status , c.file_psikotes"
        );

        $this->db->from("{$this->table_member} a"); 
        $this->db->join(
            "{$this->table_info_member} b",
            "b.id_member = a.id"
        );

        $this->db->join(
            "{$this->table_status_member} c",
            "c.id_member = a.id"
        );

        $this->db->join(
            "{$this->table_list_jobs} d",
            "d.id = c.id_jobs"
        );
        return $this->db->get()->result_array(); 
    }
    // public function select($where, $select = "*"){
    //     $this->db->select($select);
    //     $this->db->where($where);
    //     return $this->db->get($this->table)->row();
    // }

    // public function update($data, $data_where){
    //     $this->db->where($data_where);
    //     return $this->db->update($this->table, $data);
    // }

    public function save_data_member($data){
        return $this->db->insert($this->table_status_member,$data);
    }

    public function save_data($data){
        return $this->db->insert($this->table_data,$data);
    }
}