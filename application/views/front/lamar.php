<style>
    label{
        font-size:18px;
        font-weight : bold;
    }
</style>

<?= $this->session->id_user ?>
<div class="container">
    <div class="row" style="margin-top:20px">
        <div class="col-md-3">
            <div class="card">
                <img class="card-img-top" src="<?= get_avatar($foto) ?>">
                <div class="card-body">
                    <h3 class="card-title"><?= mysession('full_name_front') ?></h3>
                    <p class="card-tect" style="color:#666"><?= mysession('email_front') ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
        <?php if($this->session->flashdata("__error")): ?>
            <div class="alert alert-danger">
                <?= $this->session->flashdata("__error") ?>
            </div>
        <?php endif; ?>
          <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
            <div class="card" style=";border-top:6px solid #4760bb;">
                <div class="card-header" style="background-color: #fff">
                    <h2 style="margin-top:10px">
                        <b><?= bahasa("Lengkapi data lamaran") ?></b>
                    </h2>
                </div>
                <div class="card-body">
                    <input type="hidden" name="id" value="<?= $id ?>">
                    <?php foreach(back_to_array_list_doc($row->doc) as $val): ?>
                        <div class="form-group">
                            <label for="<?= $val ?>"><?=  get_list_doc($val, 'name') ?></label>
                            <input name="<?= $val ?>" type="file" class="form-control-file" id="<?= $val ?>">
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary btn-lg"><?= bahasa('submit') ?></button>
                </div>
            </div>
           </form>
        </div>
    </div>
</div>