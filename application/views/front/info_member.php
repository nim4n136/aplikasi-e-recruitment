<?php
    $query = $this->db->query('SELECT * FROM tbl_info_member WHERE id_member='.mysession('id_user').' ');
    $row = $query->row();
?>
<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-3">
            <div class="card">

                <img class="card-img-top" src="<?=  get_avatar($row->photo) ?>">
                <div class="card-body">
                    <h3 class="card-title"><?= mysession('full_name_front') ?></h3>
                    <p class="card-tect" style="color:#666"><?= mysession('email_front') ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
        <?php echo form_open_multipart('info_member/insert_info'); ?>
            <div class="card" style="border-top:6px solid #4760bb;">
                <div class="card-header">
                    <h4 style="margin-top:10px">
                        <b><?= bahasa('complete_info_user') ?></b>
                    </h4>
                </div>
                <div class="card-body">
                    <?php if($this->session->flashdata('__error')): ?>
                    <div class="alert alert-danger">
                        <?= $this->session->flashdata('__error') ?>
                    </div>
                    <?php endif; ?>
                    <!-- Nama lengkap -->
                    <div class="form-group">
                        <label><b><?= bahasa('full_name') ?><font color="red">*</font></b></label>
                        <input required value="<?= mysession("full_name_front") ?>" type="text" name="full_name" class="form-control" placeholder="<?= bahasa('insert_full_name') ?>">
                    </div>

                    <!-- Jenis kelamin -->
                    <div class="form-group">
                        <label><b><?= bahasa('gender') ?><font color="red">*</font></b></label>
                        <select name="jenis_kelamin" required class="custom-select">
                            <option value="" disabled selected>-- <?= bahasa("choice_gender") ?> --</option>
                            <option value="1" <?= (info_user('jenis_kelamin') == "1") ? "selected" : "" ?>><?= bahasa('gender_1') ?></option>
                            <option value="2" <?= (info_user('jenis_kelamin') == "2") ? "selected" : "" ?>><?= bahasa('gender_2') ?></option>
                        </select>
                    </div>


                    <!-- Foto & Nomer KTP -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b><?= bahasa('photo') ?><font color="red">*</font></b></label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="file_name1" id="customFile">
                                    <label class="custom-file-label" for="customFile"><?= bahasa('choose_photo')?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b><?= bahasa('no_ktp') ?><font color="red">*</font></b></label>
                                <input required value="<?= info_user("no_ktp") ?>" type="number" name="no_ktp" class="form-control" placeholder="<?= bahasa('insert_no_ktp') ?>">
                            </div>
                        </div>
                    </div>

                    <!-- Tempat Lahir & Tanggal Lahir -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b><?= bahasa('born') ?><font color="red">*</font></b></label>
                                <input value="<?= info_user("tempat_lahir") ?>" required placeholder="<?= bahasa('insert_born') ?>" type="text" class="form-control" name="tempat_lahir">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b><?= bahasa('born_date') ?></b></label>
                                <input value="<?= info_user("tanggal_lahir") ?>" required placeholder="<?= bahasa('insert_born_date') ?>" type="text" class="form-control tanggal" name="tanggal_lahir">
                            </div>
                        </div>
                    </div>   

                        <!--Umur & Status  -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b><?= bahasa('age') ?></b></label>
                                <input value="<?= info_user("umur") ?>" required placeholder="<?= bahasa('insert_age') ?>" type="number" class="form-control" name="umur">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b><?= bahasa('status') ?></b></label>
                                <select required class="custom-select" name="status">
                                    <option value="" disabled selected>-- <?= bahasa("choice_status") ?> --</option>
                                    <option value="1" <?= (info_user('status') == "1") ? "selected" : "" ?>><?= bahasa('status_1') ?></option>
                                    <option value="2" <?= (info_user('status') == "2") ? "selected" : "" ?>><?= bahasa('status_2') ?></option>
                                </select>
                            </div>
                        </div>
                    </div>    

                    <!--Pendidkan & Tahun lulus  -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b><?= bahasa('Pendidikan') ?></b></label>
                                <input value="<?= info_user("pendidikan") ?>" required placeholder="<?= bahasa('Masukan Pendidikan') ?>" type="text" class="form-control" name="pendidikan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b><?= bahasa('Tahun Lulus') ?></b></label>
                                <input name="tahun_lulus" value="<?=info_user("tahun_lulus");?>" required type="text" class="form-control tanggal" placeholder="<?= bahasa("Masukan Tahun Lulus") ?>">
                            </div>
                        </div>
                    </div>    

                    <!-- Jurusan -->
                    <div class="form-group">
                        <label><b><?= bahasa('Jurusan') ?></b></label>
                        <input required name="jurusan" value="<?= info_user("jurusan") ?>" type="text" class="form-control" placeholder="<?= bahasa("Masukan Jurusan") ?>">
                    </div>

                    <!-- Agama -->
                    <div class="form-group">
                        <label><b><?= bahasa('religion') ?></b></label>
                        <select required class="custom-select" name="agama">
                            <option value="" disabled selected>-- <?= bahasa("choice_religion") ?> --</option>
                            <option value="Islam" <?= (info_user('agama') == "Islam") ? "selected" : "" ?>>Islam</option>
                            <option value="Protestan" <?= (info_user('agama') == "Protestan") ? "selected" : "" ?>>Protestan</option>
                            <option value="Katolik" <?= (info_user('agama') == "Katolik") ? "selected" : "" ?>>Katolik</option>
                            <option value="Hindu" <?= (info_user('agama') == "Hindu") ? "selected" : "" ?>>Hindu</option>
                            <option value="Budha" <?= (info_user('agama') == "Budha") ? "selected" : "" ?>>Budha</option>
                            <option value="Konghucu" <?= (info_user('agama') == "Konghucu") ? "selected" : "" ?>>Konghucu</option>
                        </select>
                    </div> 
                            <!-- Warga negara -->
                    <div class="form-group">
                        <label><b><?= bahasa('citizen') ?></b></label>
                        <input required value="<?= info_user("warga_negara") ?>" type="text" name="warga_negara" class="form-control" placeholder="<?= bahasa('insert_citizen') ?>">
                    </div>

                        <!-- Alamat tetap -->
                    <div class="form-group">
                        <label><b><?= bahasa('address_1') ?></b></label>
                        <textarea required name="alamat_tetap" class="form-control" placeholder="<?= bahasa('insert_address_1') ?>"><?= info_user("alamat_tetap") ?></textarea>
                    </div>
                    
                        <!-- Alamat sekarang -->
                    <div class="form-group">
                        <label><b><?= bahasa('address_2') ?></b></label>
                        <textarea name="alamat_sekarang"class="form-control" placeholder="<?= bahasa('insert_address_2') ?>"><?= info_user("alamat_tetap") ?></textarea>
                    </div>

                        <!-- Golongan darah -->
                    <div class="form-group">
                        <label><b><?= bahasa('blood_group') ?></b></label>
                        <input required name="gol_darah" class="form-control" placeholder="<?= bahasa('insert_blood_group') ?>" value="<?= info_user("gol_darah") ?>">
                    </div>

                    <!-- Tinggi badan & Berat Badan -->
                    <div class="row" >
                        <div class="col-md-6">
                            <label><b><?= bahasa('hight_body') ?></b></label>
                            <input required type="number" name="tinggi_badan" class="form-control" placeholder="<?= bahasa('insert_hight_body') ?>" value="<?= info_user("tinggi_badan") ?>">
                        </div>
                        <div class="col-md-6">
                            <label><b><?= bahasa('weight_body') ?></b></label>
                            <input required type="number" name="berat_badan" class="form-control" placeholder="<?= bahasa('insert_weight_body') ?>" value="<?= info_user("berat_badan") ?>">
                        </div>
                        <div class="col-md-6 mt-4">
                            <label><b>File kemampuan <font color="red">*</font></b></label>
                            <?php if(info_user("file_kemampuan")): ?>
                            <a href="<?= base_url()."assets/foto_member/".info_user("file_kemampuan") ?>">
                                View
                            </a>
                        <?php endif ?>
                            <input required type="file" name="file_name2" class="form-control file_name2" placeholder="<?= bahasa('insert_experience_file') ?>" value="<?= info_user("file_kemampuan") ?>">
                        </div> 


                    </div>

                        <!-- Alamat sekarang -->
                        <div class="form-group mt-4">
                            <label><b><?= bahasa('motivasi') ?></b>  <font color="red">*</font></label>
                            <textarea required style="min-height:100px" name="motivasi"class="form-control"><?= info_user("motivasi") ?></textarea>
                            <?= bahasa('insert_motivasi') ?>
                        </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-lg"><?= bahasa('save') ?></button>
                </div>
            </div>
        <?= form_close(); ?>
        </div>
    </div>
</div>