<main role="main">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <?php $count = count($slider->result_array()); ?>
        <ol class="carousel-indicators">
            <?php for($i=0;$i< $count;$i++): ?>
                <li data-target="#myCarousel" data-slide-to="<?= $i ?>" class=<?= ($i == 0) ? "active" : ""?>></li>
            <?php endfor; ?>
        </ol>
        <div class="carousel-inner">
            <?php
            $i = 0;
            foreach($slider->result_array() as $slide): $i++;?>
                <div class="carousel-item <?= ($i == 1) ? "active" : ""?>">
                    <img class="second-slide" src="<?= image_slider($slide['pic'])?>">
                    <div class="container">
                        <div class="carousel-caption text-left">
                            <h1><?= $slide['title']?></h1>
                            <p><?= $slide['description']?></p>
                            <p><a class="btn btn-lg btn-primary" href="<?= site_url('home/list') ?>" role="button">
                                    <?= bahasa('Cari Pekerjaan') ?>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="container">
            <h3 class="text-center"><b><font color="orange"><?= bahasa("JOBS DI") ?> PT. SBS</b></font></h3>
            <hr>
        
        <form>
            <div class="row">
                <div class="col-4">
                    <input type="text" class="form-control" placeholder="<?= bahasa('Cari lowongan pekerjaan')?>">
                </div>
                <div class="col-4">
                    <button type="submit" class="btn btn-outline-primary mb-2"> <i class="fa fa-search"></i> <?= bahasa('Cari')?></button>
                </div>
            </div>
        </form>
        <div>
            <div class="card">
                <?php foreach($jobs->result_array() as $val): ?>
                    <div class="card-body" style="padding-left:40px;padding-right:40px;">
                        <div class="row justify-content-between">
                            <div class="col-xs-8">   
                                <h4 class="card-title"><?= $val['name']?></h4>
                                <h6 class="card-subtitle mb-2 text-muted">
                                    <span class="mr-2"><i class="fa fa-graduation-cap"></i><?= $val['academic'] ?></span>
                                    <span class="mr-2"><i class="fa fa-user"></i> Max <?= $val['max_age'] ?> <?= bahasa('Tahun') ?></span>
                                    <span class="mr-2"><i class="fa fa-suitcase"></i> <?= bahasa('Pengalaman') ?> <?= $val['experience'] ?> <?= bahasa('Tahun') ?></span>
                                </h6>
                            </div>
                            <div class="col-xs-4">
                                <a href="<?= site_url('home/detail/'.$val['id']) ?>" style="margin-top:10px" class="btn btn-primary">
                                    <i class="fa fa-briefcase"></i> <?= bahasa('Lihat & Lamar')?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <hr>
        <a href="<?= site_url('home/list')?>" class="btn btn-info"><?= bahasa('Lihat semua lowongan') ?> >> </a>
    </div>
    <hr>