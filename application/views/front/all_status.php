
<div class="container">
    <h2 style="margin-top:20px"><?= bahasa('Informasi semua status pelamar')?></h2>

<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col"><?=  bahasa('full_name') ?></th>
      <th scope="col"><?= bahasa('gender')?></th>
      <th scope="col"><?= bahasa('born_date')?></th>
      <th scope="col"><?= bahasa('address_2_1')?></th>
      <th scope="col"><?= bahasa('Posisi / Nama Pekerjaan')?></th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($all_data as $key => $row): ?>
    <tr>
      <th scope="row">1</th>
      <td><?=  $row->full_name  ?></td>
      <td><?= jenis_kelamin($row->jenis_kelamin) ?></td>
      <td><?= ($row->tanggal_lahir) ?></td>
      <td><?= $row->alamat_sekarang ?></td>
      <td><?= $row->name ?></td>
      <td> <?= status_boost4( $row->status )?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
