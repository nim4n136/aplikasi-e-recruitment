<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kartu untuk mengikuti psikotes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <style>
        table td{
            padding-top:20px;
            font-size:22px;
        }
        table th{
            padding-top:20px;
            font-size:22px;
        }
    </style>
</head>
<body onload="window.print()">

<div class="container" >
        <div class="row" style="margin-top:20px">
            <div class="col-md-12">
                <img class="rounded float-left mr-4" src="<?= base_url("/assets/logo.png") ?>" width="80" height="80">
                <div style="margin-top:15px;" class="text-center">
                    <h2 >PT. SBS </h2>
                    <span style="font-size:20px" >Kartu Perserta Calon Pegawai Baru</span>
                </div>
            </div>
        </div>
</div>
<hr>
<div class="container" style="margin-top:50px;">
    <div class="row">
    <div class="col-md-9">
        <table>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <th><?= $full_name ?></th>
            </tr>
            <tr>
                <td>Tempat tanggal lahir</td>
                <td> : </td>
                <th><?= $tempat_lahir ?>, <?= $tanggal_lahir ?></th>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td> : </td>
                <th><?= jenis_kelamin($jenis_kelamin)?></th>
            </tr>
            <tr>
                <td>Posisi</td>
                <td> : </td>
                <th><?= $name?></th>
            </tr>
        </table>
    
    </div>
    <div class="col-md-3">
        <img src="<?=get_avatar($photo)?>" class="img-fluid img-thumbnail"  alt="">
    </div>
    </div>
    <div style="margin-top:90px;">
        <hr >
        <p>Keterangan : Kartu ini wajib di bawa untuk mengikuti psikotes</p>
    </div>
</div>
</div>
</body>
</html>