<?php 
$lg= isset($_GET['lg']) ? $_GET['lg'] :'';
if ($lg =='blokir') {
  echo "<div class='alert alert-danger'>Akses Di Blokir Silahkan Hubunugi Sys admin untuk informasi lebih lanjut</div>";
}else{

}

?>

<div class="container" style="margin-top:30px;">
  <div class="card " style="margin:auto;max-width: 430px;border-top:6px solid #4760bb;">
    <div class="card-header" style="background-color: #fff">
      <h2 style="margin-top:10px"><b><?= bahasa('Silahkan masuk')?></b></h2>
    </div>
    <div class="card-body">
        <?php if($this->session->userdata('confirm_email_success')): ?>
          <div class="alert alert-success"><?= $this->session->userdata('confirm_email_success') ?></div>
        <?php endif;?>
        <?php if($this->session->flashdata('error_login')): ?>
          <div class="alert alert-danger" role="alert">
            <?= $this->session->flashdata('error_login') ?>
          </div>
        <?php  endif;?>
        <form action="<?= $action_login ?>" method="POST">
            <div class="form-group">
              <label for="exampleInputEmail1"><b><?= bahasa('email_address') ?></b></label>
              <input type="email" name="email" class="form-control form-control-lg" placeholder="<?= bahasa('insert_email_address') ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1"><b><?= bahasa('password') ?></b></label>
              <input type="password" name="password" class="form-control form-control-lg" placeholder="<?= bahasa('insert_password') ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1"><b>Captcha</b></label>
              <br/>
              <?= $captcha ?>
            </div>
            <div class="form-group">
              <input type="text" class="form-control form-control-lg" name="captcha" placeholder="<?= bahasa('insert_code_captcha') ?>">
            </div>
            <hr>
            <button type="submit" class="btn btn-primary btn-lg btn-block"><?= bahasa('Masuk') ?></button>
            <hr>
            <a href="<?= $link_register ?>" class='btn btn-success btn-lg btn-block'>
              <?= bahasa('register') ?>
            </a>
        </form>
    </div>
  </div>
</div>