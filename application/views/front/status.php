
<div class="container">
    <div class="card" style="margin-top:30px;margin-bottom:200px;">
        <div class="card-header">
            <h3 class="card-title"><?= bahasa('Status Lamaran anda')?></h3>
        </div>
        <div class="card-body" style="padding-left:40px;padding-right:40px;">
            <span style="color:#777"><?= bahasa('Posisi / Nama Pekerjaan')?></span>
            <h3><?= $name ?></h3>
                <h6 class="card-subtitle mt-1 mb-2 text-muted">
                <span class="mr-2"><i class="fa fa-graduation-cap"></i> <?= $academic ?></span>
                <span class="mr-2"><i class="fa fa-user"></i> Max <?= $max_age ?>  <?= bahasa('Tahun')?></span>
                <span class="mr-2"><i class="fa fa-suitcase"></i> <?= bahasa('Pengalaman')?> <?= $experience ?>  <?= bahasa('Tahun')?></span>
            </h6>
            <h5>
                <span style="color:#777"><?= bahasa('Status')?> : </span>
                <?= status_boost4( $status )?>
            </h5>
            <?php $decode = json_decode($file_psikotes) ?>

            <?php if(@$decode->keterangan != false): ?>
                <hr>
                <h4><?= bahasa('Informasi') ?></h4>
                <p><?= $decode->keterangan ?></p>
            <?php endif; ?>
            
            <?php if($status == "accepted_1"): ?>
                <hr>
                <p style="color:#777">
                    <?= bahasa('Silahkan print / save dokumen untuk mengikuti psikotes') ?>
                </p>
                <a target="__blank" href="<?= site_url('lamar/print') ?>" class="btn btn-primary"> <i class="fa fa-print"></i> Print / Download</a>
            <?php elseif($status == "accepted_2" or $status == "rejected_2"): ?>
                <hr>
                <a target="__blank" href="<?= base_url('assets/data_lamar/'.$decode->file_name) ?>" class="btn btn-primary"> 
                    <i class="fa fa-download"></i>  <?= bahasa('Download hasil test psikotes') ?>
                </a>
            <?php endif; ?>
        </div>
        
    </div>
</div>