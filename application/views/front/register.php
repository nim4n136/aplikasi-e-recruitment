
<div class="container" style="margin-top:30px;">
  <div class="card " style="margin:auto;max-width: 500px;border-top:6px solid #4760bb;">
    <div class="card-header" style="background-color: #fff">
      <h2 style="margin-top:10px"><b><?= bahasa('register_account') ?></b></h2>
    </div>
    <div class="card-body">
        <?php if($this->session->flashdata('error_register')): ?>
          <div class="alert alert-danger" role="alert">
            <?= $this->session->flashdata('error_register') ?>
          </div>
        <?php elseif($this->session->flashdata('success_register')): ?>
          <div class="alert alert-success" role="alert">
            <?= $this->session->flashdata('success_register') ?>
          </div>
        <?php  endif;?>
        <form method="post" action="<?= $action_register ?>">
            <div class="form-group">
              <label for=""><b><?= bahasa('full_name') ?></b></label>
              <input type="text" class="form-control form-control-lg" name="full_name" placeholder="<?= bahasa('insert_full_name') ?>">
            </div>
            <div class="form-group">
              <label for=""><b><?= bahasa('email_address') ?></b></label>
              <input type="email" class="form-control form-control-lg" name="email"  placeholder="<?= bahasa('insert_email_address') ?>">
            </div>
            <div class="form-group">
              <label for=""><b><?= bahasa('no_hp') ?></b></label>
              <input type="text" name="no_hp" class="form-control form-control-lg"  placeholder="<?= bahasa('insert_no_hp') ?>">
            </div>
            <div class="form-group">
            <label for=""><b>Captcha </b></label>
              <br/>
             <?= $captcha ?>
            </div>
            <div class="form-group">
             <input type="text" class="form-control form-control-lg" name="captcha" placeholder="<?= bahasa('insert_code_captcha') ?>">
            </div>
            <hr>
            <button type="submit" class="btn btn-primary btn-lg"><?= bahasa('register_now') ?></button>
          </form>
    </div>
  </div>
</div>