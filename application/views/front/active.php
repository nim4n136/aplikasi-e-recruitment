<?php  if(!$exists): ?>
<div style="padding-bottom:130px">
    <div class="card " style="margin:auto;margin-top:40px;max-width: 500px;">
        <div class="card-header">
            <h2><b>403</b></h2>
        </div>
        <div class="card-body">
            <?= bahasa("token_has_been_expire") ?>
        </div>
    </div>
</div>
<?php else: ?>
<div style="padding-bottom:130px;margin:auto;margin-top:40px;max-width: 500px;">
    <?php if($error): ?>
        <div class="alert alert-danger">
            <?= $error ?>
        </div>
    <?php else: ?>
        <div class="alert alert-primary">
            <?= bahasa('create_new_password_for_active_account') ?>
        </div>
    <?php endif; ?>
    <div class="card" style="">
        <div class="card-header">
            <h2><b><?= bahasa('create_new_password') ?></b></h2>
        </div>
        <div class="card-body">
        <form method="post" >
            <div class="form-group">
              <label for=""><b><?= bahasa('password') ?></b></label>
              <input type="password" class="form-control form-control-lg" name="password" placeholder="<?= bahasa('insert_password') ?>">
            </div>
            <div class="form-group">
              <label for=""><b><?= bahasa('confirm_password') ?></b></label>
              <input type="password" class="form-control form-control-lg" name="confirm_password"  placeholder="<?= bahasa('insert_confirm_password') ?>">
            </div>
            <hr>
            <button type="submit" class="btn btn-primary btn-lg"><?= bahasa('submit') ?></button>
          </form>
        </div>
    </div>
</div>
<?php endif; ?>