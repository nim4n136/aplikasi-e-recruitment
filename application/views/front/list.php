
<main role="main">
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
  <div class="container">
    <h2><?= bahasa('Semua lowongan pekerjaan')?></h2>
  </div>
</div>
</main>
<div class="container">
            <form action="" method="post">
                <div class="row">
                    <div class="col-4">
                        <input type="text" class="form-control" placeholder="<?= bahasa('Cari lowongan pekerjaan') ?>" name="q"
                        value="<?= $q ?>">
                    </div>
                    <div class="col-4">
                            <button type="submit" class="btn btn-outline-primary mb-2"> <i class="fa fa-search"></i> 
                            <?= bahasa('Cari')?></button>
                    </div>
                </div>
            </form>
        <div>
            <div class="card">
                <?php foreach($data->result_array() as $val): ?>
                    <div class="card-body" style="padding-left:40px;padding-right:40px;">
                        <div class="row justify-content-between">
                            <div class="col-xs-8">   
                                <h4 class="card-title"><?= $val['name']?></h4>
                                <h6 class="card-subtitle mb-2 text-muted">
                                    <span class="mr-2"><i class="fa fa-graduation-cap"></i><?= $val['academic'] ?></span>
                                    <span class="mr-2"><i class="fa fa-user"></i> <?= bahasa('Max')?> <?= $val['max_age'] ?> <?= bahasa('Tahun')?></span>
                                    <span class="mr-2"><i class="fa fa-suitcase"></i> <?= bahasa('Pengalaman')?> <?= $val['experience'] ?> <?= bahasa('Tahun')?></span>
                                </h6>
                            </div>
                            <div class="col-xs-4">
                                <a href="<?= site_url('home/detail/'.$val['id']) ?>" style="margin-top:10px" class="btn btn-primary">
                                    <i class="fa fa-briefcase"></i> 
                                    <?= bahasa('Lihat & Lamar') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <nav style="margin-top:30px">        
            <?= $pagination ?>
        </nav>
    </div>
</div>