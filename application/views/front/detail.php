<div class="container">
    <br>
    <div class="card" style="border-radius:x">
        <div class="card-body" style="padding-left:40px;padding-right:40px;">
            <div class="row justify-content-between">
                <div class="col-xs-8">   
                    <h4 class="card-title"><?= $data_job->name ?></h4>
                    <h6 class="card-subtitle mb-2 mt-2 text-muted">
                        <span class="mr-2"><i class="fa fa-graduation-cap"></i> <?= $data_job->academic ?></span>
                        <span class="mr-2"><i class="fa fa-user"></i> Max <?= $data_job->max_age ?> <?= bahasa('Tahun') ?></span>
                        <span class="mr-2"><i class="fa fa-suitcase"></i> <?= bahasa('Pengalaman') ?> <?= $data_job->experience ?> <?= bahasa('Tahun')?></span>
                    </h6>
                </div>
                <div class="col-xs-4">
                    <a href="<?= site_url('lamar/'.$data_job->id) ?>" style="margin-top:10px" class="btn btn-primary">
                        <i class="fa fa-briefcase"></i> <?= bahasa('Lamar') ?> 
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body" style="padding-left:40px;padding-right:40px;">
            <div class="row ">
                <h5 class="card-title" style="color:#888;font-size:20px">
                    <i class="fa fa-edit"></i> 
                    <?= bahasa('Deskripsi Pekerjaan') ?>
                </h5>
            </div>  
            <div style="margin-left:20px">
                <?= $data_job->description ?>
            </div>
        </div>
    </div>
</div>