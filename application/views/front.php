<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- <link rel="icon" href="<?= base_url() ?>assets/logo.png" type="image/x-icon" /> -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <title><?= isset($title) ? $title : "" ?></title>
  </head>
  <body>

<header>
    <div style="background: #4760bb;" class="topnav">
    <!-- #4760bb -->
        <div class="container" >
             <div class="row justify-content-between" style="padding:8px;">
                <div class="col-8">
                <!-- <img class="rounded float-left mr-4" src="<?= base_url("/assets/logo.png") ?>" width="60" height="40"> -->
                        <a href=""  style="color:white"><i class="fa fa-phone mr-2"></i> <?= get_setting('phone') ?></a>
                        <a href="" class="ml-4 text-white"><i class="fa fa-envelope-o mr-2"></i><?= get_setting('email') ?></a>
                    </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                    <a href="<?= get_setting('facebook') ?>" class="text-white mr-4">
                        <i class="fa fa-facebook"></i> 
                    </a>
                    <a href="<?= get_setting('twitter') ?>" class="text-white mr-4">
                        <i class="fa fa-twitter"></i> 
                    </a>
                    <a href="<?= get_setting('youtube') ?>" class="text-white mr-4">
                        <i class="fa fa-youtube"></i> 

                    </a>

                </div>
            </div>    
        </div>
    </div>

<nav class="navbar navbar-expand-lg navbar-light bg-light"> <div class="container">

   <a class="navbar-brand" href="<?= $link_home ?>"><b><?= get_setting('brand') ?></b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
      </button>
<right>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">

    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="btn btn-outline-success mr-2" href="<?= site_url('lamar/all_status')?>">
            <?= bahasa('Semua Informasi') ?>
             
        </a>
      </li>

    <!-- </ul> -->
  
   
    <?php  if(mysession('id_user')): ?>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?= site_url("/info_member")?>">Setting</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?= site_url("/home/logout") ?>">Keluar</a>
        </div>
        <li class="nav-item">
        ​<picture>
            <img style="border-radius:50%;  width:40px;height:40px;    object-fit: cover;" src="<?= get_avatar() ?>" class="img-fluid ">
        </picture>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="#" style="font-size:18px">
                <b><?= mysession('full_name_front') ?></b>
            </a>
        </li>
    <?php else: ?>
        <li class="nav-item">
            <a href="<?= $link_register ?>" class="btn btn-outline-success mr-2" ><?= bahasa('Daftar') ?></a>
            <a href="<?= $link_login ?>" class="btn btn-outline-primary" ><?= bahasa('Masuk')?></a>
             <a href="<?= $organitation_structure ?>" class="btn btn-outline-primary" ><?= bahasa('Struktur organisasi')?></a>
        
        </li>
         <ul class="navbar-nav">

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="<?= site_url('home/change_lang') ?>?lang=id">Indonesia</a>
                <a class="dropdown-item" href="<?= site_url('home/change_lang') ?>?lang=en">English</a>
            </div>

            <li class="nav-item">
                <a class="nav-link active" href="#" style="font-size:18px">
                   <?= bahasa('Bahasa')?> <i class="fa fa-language"></i> 
                </a>
            </li>

        </li>
    <?php endif; ?>
    </ul>
  </div>
</div></nav>
</header>
<?= $contents ?>
<footer class="section footer-classic context-dark bg-image" style="color:white;background:#343a40!important;margin-top:80px;">
<div class="container">
    <div class="row row-30" style="padding-top:50px;">
    <div class="col-md-4">
        <div class="pr-xl-4">
        <h4>About </h4>
        <p><?= get_setting( 'about' )?></p>
        <p class="rights"><span>©  </span><span class="copyright-year">2018</span><span> </span><span>IT-SBS</span><span>. </span><span>All Rights Reserved.</span></p>
        </div>
    </div>

    <div class="col-md-4">
        <div class="pr-xl-4">
        <h4>News </h4>
        <p><?= get_setting('new')?></p>
        </div>
    </div>
    <div class="col-md-4">
        <h4>Contact </h4>

        <dl class="contact-list">
        <dt>Alamat:</dt>
        <dd><?= get_setting('address')   ?></dd>
        </dl>
        <dl class="contact-list">
        <dt>email:</dt>
        <dd><a href="mailto:<?= get_setting('email')   ?>">
        <?= get_setting('email')   ?>
            </a></dd>
        </dl>
        <dl class="contact-list">
        <dt>phones:</dt>
        <dd><a href="#">
            <?= get_setting('phone')   ?>
        </a></dd>
        </dl>
    </div>
    </div>
</div>
</footer>
</main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/holder.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(function(){
            $(window).on('scroll', function (e) {
                $('.topnav').toggleClass('fixed-top', $(window).scrollTop() > 150);
            });
            $('.tanggal').datepicker({
                format: "dd/mm/yyyy",    
                autoclose: true
            });
        })
    </script>
  </body>
</html>
