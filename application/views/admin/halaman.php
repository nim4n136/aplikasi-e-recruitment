<div class="content-wrapper">

    <section class="content">
        <div class="box box-success box-solid">

<?php if($form =='y'): ?>

            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-key"></i>Edit My Profile</h3>
            </div>
   
<?= $this->session->flashdata('pesan')  

 ?>
<table class="table table-striped">
<form action="" method="POST" enctype="multipart/form-data">
<tr><td>Judul Halaman<small></small></td><td><input type='text' name="judul" class="form-control" required="" value="<?= $judul_j ?>"></td></tr>

<tr><td>Isi</td><td><textarea class="ckeditor form-control" name="isi"><?= $isi ?></textarea></td></tr>
<tr><td></td><td><button class="btn btn-success" name="kirim"><i class="fa fa-save"></i>Save Data</button></td></tr>	
</form>
</table>  

<?php elseif($form =='n'): ?>
    <br />
<a href="<?= base_url('index.php/halaman/managable/add') ?>" class="btn btn-success">Tambah Data</a>
<br />
<div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <table class="table table-bordered table-striped" id="mytable">
                                <thead>
                                    <tr>
                                        <th width="30px">No</th>
                                        <th>Judul</th>
                                        <th>User </th>
                                        <th width="100px">Action</th>
                                    </tr>
                                    <?php
                                    $no = 1;
                                    foreach ($data->result() as $m) {
                                        $nama =$this->db->get_where('tbl_user',array('id_user_level'=>$m->id_user))->row_array();
                                        echo "<tr>
                        <td>$no</td>
                        <td>$m->judul</td>
                         <td>".$nama['full_name']."</td>
                        <td><a href='".base_url('index.php/halaman/managable/edit/'.$m->id)."' class='btn btn-primary'><i class='fa fa-edit'></i><a>
                        <a href='".base_url('index.php/halaman/managable/delete/'.$m->id)."' class='btn btn-danger'><i class='fa fa-trash'></i><a>

                        </td>
                        </tr>";
                                        $no++;
                                    }
                                    ?>
                                </thead>
                                <!--<tr><td></td><td colspan="2">
                                        <button type="submit" class="btn btn-danger btn-sm">Simpan Perubahan</button>
                                    </td></tr>-->

                            </table>
                        </div>
                        
                    </div>

<?php endif; ?>


</div>
</section>
</div>

<script type="text/javascript">
     $(function(){ 
     $('#mytable').DataTable({

       });
     }); 

</script>