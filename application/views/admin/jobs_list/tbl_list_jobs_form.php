<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA JOBS</h3>
            </div>
			<div class="box-body">
            <form action="<?= $action; ?>" method="post">
			<div class="form-group">
				<label for="">Name <?= form_error('name') ?> </label>
				<input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?= $name; ?>" />  
			</div>

			<div class="form-group">
				<label for="">Description <?= form_error('description') ?>  </label>
				<textarea class="textarea form-control" rows="7" name="description" id="description" placeholder="Description"><?= $description; ?></textarea>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Date Active <?= form_error('date_active') ?></label> 
						<input type="text" class="form-control tanggal" name="date_active" id="date_active" placeholder="Date Active" value="<?= date_en_id($date_active); ?>" />  
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Date Expire <?= form_error('date_expire') ?> </label>
						<input type="text" class="form-control tanggal" name="date_expire" id="date_expire" placeholder="Date Expire" value="<?= date_en_id($date_expire); ?>" />  
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Max Age (Years) <?= form_error('max_age') ?></label>
						<input type="text" class="form-control" name="max_age" id="max_age" placeholder="Max Age" value="<?= $max_age; ?>" /> 
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Academic <?= form_error('academic') ?></label>
						<input type="text" class="form-control" name="academic" id="academic" placeholder="Academic" value="<?= $academic; ?>" />  
					</div>
				</div> 
			</div>
			<div class="form-group">
				<label for="">Experience (Years) <?= form_error('experience') ?></label>
				<input type="text" class="form-control" name="experience" id="experience" placeholder="Experience" value="<?= $experience; ?>" />  
			</div>
			<div class="form-group">
				<label for="">Salary <?= form_error('salary') ?> </label>
				<input type="text" class="form-control" name="salary" id="salary" placeholder="Salary" value="<?= $salary; ?>" />  
			</div>
			<!-- <div class="form-group">
				<label for="toefl">Required Document <?= form_error('doc') ?></label> 
				<input type="text" class="form-control" name="toefl" id="toefl" placeholder="Toefl" value="<?= $toefl; ?>" />  
			</div> -->
			<div class="form-group">
			<?php $doc_arr = back_to_array_list_doc($doc); ?>
			<label for="doc">Required Document <?= form_error('doc') ?></label> 
                <select name="doc[]" value="Dasda" id="doc" class="form-control select2" multiple="multiple" data-placeholder="Required Document" style="width: 100%;">
                  	<?php foreach(list_doc() as $key => $value): ?>
				  		<option <?= (in_array($key, $doc_arr)) ?  "selected" : "" ?> value="<?= $key?>"><?= $value['name'] ?></option>
					<?php endforeach; ?>
                </select>
              </div>
			<input type="hidden" name="id" value="<?= $id; ?>" /> 
	    	<button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i> <?= $button ?></button> 
	    	<a href="<?= site_url('admin/jobs_list') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a>  
			</form>    
		</div>    
	</div>
</div>
</div>