<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Tbl_list_jobs Read</h2>
        <table class="table">
	    <tr><td>Name</td><td><?php echo $name; ?></td></tr>
	    <tr><td>Date Active</td><td><?php echo $date_active; ?></td></tr>
	    <tr><td>Date Expire</td><td><?php echo $date_expire; ?></td></tr>
	    <tr><td>Max Age</td><td><?php echo $max_age; ?></td></tr>
	    <tr><td>Academic</td><td><?php echo $academic; ?></td></tr>
	    <tr><td>Experience</td><td><?php echo $experience; ?></td></tr>
	    <tr><td>Salary</td><td><?php echo $salary; ?></td></tr>
	    <tr><td>Description</td><td><?php echo $description; ?></td></tr>
	    <tr><td>Toefl</td><td><?php echo $toefl; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('admin/jobs_list') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>