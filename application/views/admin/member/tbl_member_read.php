<div class="content-wrapper">
    
    <section class="content">
        <div class="box box-primary ">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah data pelamar</h3>
            </div>
        <table class="table">
	    <tr><td>Full Name</td><td><?php echo $full_name; ?></td></tr>
	    <tr><td>Email</td><td><?php echo $email; ?></td></tr>
    	    <tr><td>No Handphone</td><td><?php echo $no_handphone; ?></td></tr>
	    <tr><td>Verify Email</td><td><?php echo $verify_email; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('admin/member') ?>" class="btn btn-default">Cancel</a></td></tr>
</table>             
</div>
</section>
</div>
