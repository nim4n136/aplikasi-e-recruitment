<!doctype html>
<html>
    <head>
        <title>Data member</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Tbl_member List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Full Name</th>
		<th>Email</th>
		<th>No Handphone</th>
		<th>Verify Email</th>
		
            </tr><?php 
            foreach ($tbl_member_data as $member)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $member->full_name ?></td>
		      <td><?php echo $member->email ?></td>
		      <td><?php echo $member->no_handphone ?></td>
		      <td><?php echo $member->verify_email ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>