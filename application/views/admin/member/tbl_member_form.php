<div class="content-wrapper">
    
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah data pelamar</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">
<table class='table table-bordered>'>

	    <tr><td width='200'>Full Name <?php echo form_error('full_name') ?></td><td><input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name" value="<?php echo $full_name; ?>" /></td></tr>
	    <tr><td width='200'>Email <?php echo form_error('email') ?></td><td><input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" /></td></tr>
	    <tr><td width='200'>Password <?php echo form_error('password') ?></td><td><input type="text" class="form-control" name="password" id="password" placeholder="Password" /></td></tr>
	    <tr><td width='200'>No Handphone <?php echo form_error('no_handphone') ?></td><td><input type="text" class="form-control" name="no_handphone" id="no_handphone" placeholder="No Handphone" value="<?php echo $no_handphone; ?>" /></td></tr>
	    <tr><td width='200'>Verify Email <?php echo form_error('verify_email') ?></td><td><input type="text" class="form-control" name="verify_email" id="verify_email" placeholder="Verify Email" value="<?php echo $verify_email; ?>" /></td></tr>
	    <tr><td width='200'>Kode Verify <?php echo form_error('kode_verify') ?></td><td><input type="text" class="form-control" name="kode_verify" id="kode_verify" placeholder="Kode Verify" value="<?php echo $kode_verify; ?>" /></td></tr>
	    <tr><td></td><td><input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i> <?php echo $button ?></button> 
	    <a href="<?php echo site_url('admin/member') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a></td></tr>
	</table></form>        </div>
</div>
</div>