<style>
    table td, table th{
        font-size:18px;
        padding-left:10px;
        padding-bottom:5px;
    }
    .header-th{
        font-size:21px;padding-top:10px;
    }
</style>
<div class="content-wrapper">
    <section class="content">
            <?php if($this->session->flashdata('__error')): ?>
                <div class="alert alert-danger"><?= $this->session->flashdata('__error') ?></div>
            <?php elseif($this->session->flashdata('__success')): ?>
                <div class="alert alert-success"><?= $this->session->flashdata('__success') ?></div>
            <?php endif; ?>
            <div class="box box-primary">
                <div class="box-header box-solid">
                    <h1 class="box-title" style="font-size:25px">Posisi yang di ingin kan</h1>
                </div>
                <div class="box-body">

                    <table>
                        <tr>
                            <td><?= bahasa("Posisi / Nama pekerjaan") ?></td>
                            <td> : </td>
                            <th><?= $name ?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Umur Makimal") ?></td>
                            <td> : </td>
                            <th><?= $max_age." ".bahasa('Tahun') ?> </th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Akademik") ?></td>
                            <td> : </td>
                            <th><?= $academic ?> </th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Pengalaman") ?></td>
                            <td> : </td>
                            <th><?= $experience." ".bahasa('Tahun')  ?> </th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Salary") ?></td>
                            <td> : </td>
                            <th><?= $salary  ?> </th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Status") ?></td>
                            <td> : </td>
                            <th><?= status($status)  ?> </th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header box-solid">
                    <h1 class="box-title" style="font-size:25px">Bio data pelamar</h1>
                </div>
                <div class="box-body">
                    <div class="col-md-9">
                    <table>
                        <tr>
                            <td><?= bahasa("full_name") ?></td>
                            <td> : </td>
                            <th><?= $full_name ?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("gender")?></td>
                            <td> : </td>
                            <th><?= jenis_kelamin($jenis_kelamin) ?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("no_ktp")?></td>
                            <td> : </td>
                            <th><?= ($no_ktp) ?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("age")?></td>
                            <td> : </td>
                            <th><?= ($umur) ?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("status")?></td>
                            <td> : </td>
                            <th><?= ($status == 1) ? "Menikah" : "Belum Menikah" ?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Tempat tanggal lahir")?></td>
                            <td> : </td>
                            <th><?= $tempat_lahir.", ".$tanggal_lahir?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("citizen")?></td>
                            <td> : </td>
                            <th><?= $warga_negara?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("religion")?></td>
                            <td> : </td>
                            <th><?= $agama?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("blood_group")?></td>
                            <td> : </td>
                            <th><?= $gol_darah?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("address_1")?></td>
                            <td> : </td>
                            <th><?= $alamat_tetap?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("address_2_1")?></td>
                            <td> : </td>
                            <th><?= $alamat_sekarang?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("hight_body")?></td>
                            <td> : </td>
                            <th><?= $tinggi_badan?> cm</th>
                        </tr>
                        <tr>
                            <td><?= bahasa("weight_body")?></td>
                            <td> : </td>
                            <th><?= $berat_badan?> kg</th>
                        </tr>
                        <tr>
                            <th class="header-th">Pendidikan</th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Pendidikan")?></td>
                            <td> : </td>
                            <th><?= $pendidikan?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Tahun Lulus")?></td>
                            <td> : </td>
                            <th><?= $tahun_lulus?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("Jurusan")?></td>
                            <td> : </td>
                            <th><?= $jurusan?></th>
                        </tr>
                        <tr>
                            <th class="header-th">Kontak</th>
                        </tr>
                        <tr>
                            <td><?= bahasa("email_address")?></td>
                            <td> : </td>
                            <th><?= $email?></th>
                        </tr>
                        <tr>
                            <td><?= bahasa("no_hp")?></td>
                            <td> : </td>
                            <th><?= $no_handphone?></th>
                        </tr>

                        <tr>
                            <th class="header-th">Lainnya</th>
                        </tr>
                        <tr>
                            <td><?= bahasa("File Kemampuan")?></td>
                            <td> : </td>
                            <th>

                        <a target="__blank" style="margin-top:2px;font-size:18px" href="<?= site_url("/assets/foto_member/".$file_kemampuan)?>"> 
                            <i class="fa fa-download"></i>   Download  </a>
                            </th>
                        </tr>
                        <tr>
                            <td><?= bahasa("motivasi")?></td>
                            <td> : </td>
                            <th><?= $motivasi?></th>
                        </tr>
                    </table>
                    </div>

                    <div class="col-md-3">
                        <img class="img-responsive img-thumbnail" src="<?= get_avatar($photo) ?>" >
                        <p style="text-align: center;">
                        <a target="__blank" style="margin-top:2px;font-size:18px" href="<?= get_avatar($photo) ?>"> 
                            <i class="fa fa-download"></i>   Download Foto </a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header box-solid">
                    <h1 class="box-title" style="font-size:25px;!important">
                        Dokumen pelamar
                    </h1>
                </div>
                <div class="box-body">
                    <table>
                        <?php foreach($data_doc as $data): ?>

                            <tr>
                                <th><?= get_list_doc($data['name'], 'name') ?></th>
                                <td> : </td>
                                <td> 
                                    <a target="__blank" href="<?= base_url("/assets/data_lamar/".str_replace(' ','_',$data['value'])) ?>"><i class="fa fa-download"></i>  Download</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>

            <form action="<?= $action_update_data ?>" method="post" enctype="multipart/form-data">
            <div class="box box-primary">
                <div class="box-header box-solid">
                    <h1 class="box-title" style="font-size:25px;!important">
                        Edit Status Pelamar
                    </h1>
                </div>
                <div class="box-body">
                <?php $ket = (json_decode($file_psikotes)) ?>
                    <div class="form-group">
                        <select id="status" name="status" class="form-control input-lg">
                            <?php foreach(data_status() as $key => $value): ?>
                            <option <?= ($status == $key) ? "selected" : "" ?> value="<?= $key ?>"><?= $value['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <input type="hidden" name="json" value="<?= base64_encode($file_psikotes) ?>">
                    <div class="form-group" id="upload_file">
                        <label for="file">Upload hasil Psikotes (Format Excel) 
                        </label>
                        <?php if($ket): ?>
                            <p>
                                <a target="__blank" href="<?= base_url("/assets/data_lamar/".$ket->file_name)?>"><?= $ket->file_name ?></a>
                            </p>
                        <?php endif;?>
                        <input name="file_psikotes" name="file" type="file" id=file class="input-lg form-control">
                    </div>
                    <div class="form-group" id="upload_file">
                        <label for="file">Informasi tambahan (Optional) </label>
                        <textarea placeholder="Masukan Informasi tambahan" class="form-control input-lg" name="keterangan" cols="10" rows="5"><?= @$ket->keterangan ?></textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-lg btn-primary">Update data</button>
                    <a href="<?= site_url('/admin/status_member/') ?>" class="btn btn-lg btn-danger">Back</a>
                </div>
            </div>
            </form>
    </section>
</div>
<script>
     $(function(){
        var value = $('#status').val();
        if(value == "accepted_2" || value == "rejected_2"){
            $('#upload_file').show()
        }else{
            $('#upload_file').hide()
        }
        $('#status').change(function() {
            var value = $(this).val();
            if(value == "accepted_2" || value == "rejected_2"){
                $('#upload_file').show()
            }else{
                $('#upload_file').hide()
            }
        });
     })
</script>
