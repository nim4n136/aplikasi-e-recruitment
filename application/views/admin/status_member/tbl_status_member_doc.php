<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Tbl_status_member List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Member</th>
		<th>Id Jobs</th>
		<th>Status</th>
		
            </tr><?php
            foreach ($admin/status_member_data as $admin/status_member)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $admin/status_member->id_member ?></td>
		      <td><?php echo $admin/status_member->id_jobs ?></td>
		      <td><?php echo $admin/status_member->status ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>