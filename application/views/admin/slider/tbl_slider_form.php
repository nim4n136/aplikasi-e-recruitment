<div class="content-wrapper">
    
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA SLIDER</h3>
            </div>
            <div class="box-body">
            <form action="<?= $action; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="">
                    Image Slider <?= form_error('pic') ?> 
                </label>
               <input type="file" name="pic" id="pic" placeholder="Pic" value="<?= $pic; ?>" />
               <p class="help-block">Silahkan pilih file image.</p>
            </div>

            <div  class="form-group">
                <label for="">
                    Bahasa <?= form_error('lang') ?> 
                </label>
                <select name="lang" class="form-control">
                    <option <?= ($lang == "id") ? "selected" : ""?> value="id">id</option>
                    <option <?= ($lang == "en") ? "selected" : ""?> value="en">en</option>
                </select>
            </div>
            <div  class="form-group">
                <label for="">
                    Title <?= form_error('title') ?> 
                </label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?= $title; ?>" />
            </div>
            <div  class="form-group">
                <label for="">
                    Description <?= form_error('description') ?>  
                </label>
                <textarea class="textarea form-control" rows="6" name="description" id="description" placeholder="Description"><?= $description; ?></textarea>
            </div>
            <input type="hidden" name="id" value="<?= $id; ?>" /> 
            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i> <?= $button ?></button> 
            <a href="<?= site_url('admin/slider') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a>
        </form>      
        </div>  
    </div>
    </section>
</div>