<div class="content-wrapper">
    <section class="content">   
    <div class="row">
        <div class="col-md-12">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                 <h2 style="margin-top:0px">View Silder</h2>
                <table class="table">
                    <tr>
                        <td> <b>Image</b>    </td>
                        <td><img src="<?= image_slider($pic); ?>" class="img-responsive" alt=""></td>
                    </tr>
                    <tr>
                        <td><b>Title</b></td>
                        <td><?= $title; ?></td>
                    </tr>
                    <tr>
                        <td><b>Description</b></td>
                        <td><?= $description; ?></td>
                    </tr>
                    <tr>
                        <td><b>Bahasa</b></td>
                        <td><?= $lang; ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a href="<?= site_url('admin/slider') ?>" class="btn btn-default">Cancel</a></td>
                    </tr>

                </table>
            </div>
        </div>
        </div>
    </div>
    </section>
</div>