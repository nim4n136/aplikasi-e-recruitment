<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Pengaturan
      </h1>
      
    </section>
    <section class="content">
    <div class="row">
        <div class="col-md-7">
        <?php if($msg): ?>
            <div class="alert alert-<?= ($error) ? "danger" : "success" ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?= ($error) ? "ban" : "check" ?>"></i> <?= ($error) ? "Error !" : "Success" ?></h4>
                <p><?= $msg ?></p>
            </div>
<?php endif;     ?>

        <form action="<?= site_url('admin/settings/save')?>" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Umum</h3>
            </div>
              <div class="box-body">
                <div class="form-group">
                  <label >Brand</label>
                  <input value="<?= get_setting("brand") ?>" name="brand" type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input value="<?= get_setting("address") ?>" name="address" type="text" class="form-control" >
                </div>
                <div class="form-group">
                  <label>Tentang</label>
                  <textarea class="form-control" name="about" ><?= get_setting("about") ?></textarea>
                </div>
                <div class="form-group">
                  <label>New</label>
                  <textarea class="form-control" name="new" ><?= get_setting("new") ?></textarea>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input value="<?= get_setting("email") ?>" name="email" type="text" class="form-control" >
                </div>
                <div class="form-group">
                  <label>Phone</label>
                  <input name="phone" type="text" class="form-control" value="<?= get_setting("phone") ?>" >
                </div>
              </div>
            <div class="box-header">
              <h3 class="box-title">Link Sosial Media</h3>
            </div>
              <div class="box-body">
                <div class="form-group">
                  <label>Facebook </label>
                  <input name="facebook" type="text" class="form-control" value="<?= get_setting("facebook") ?>">
                </div>
                <div class="form-group">
                  <label>Youtube</label>
                  <input name="youtube" type="text" class="form-control" value="<?= get_setting("youtube") ?>">
                </div>
                <div class="form-group">
                  <label>Twitter</label>
                  <input name="twitter" type="text" class="form-control" value="<?= get_setting("brand") ?>">
                </div>
              </div>
            <div class="box-header">
              <h3 class="box-title">Bahasa</h3>
            </div>
              <div class="box-body">
                <div class="form-group">
                  <select name="lang_default" class="form-control">
                    <option value="id" <?= (get_setting("lang_default") == "id") ? "selected"  : "" ?>>Bahasa indonesia</option>
                    <option value="en" <?= (get_setting("lang_default") == "en") ? "selected"  : "" ?>>English</option>
                  </select>
                </div>
            </div>
            <div class="box-footer">
                <button class="btn btn-primary" type="submit">Simpan</button>
            </div>
          </div>
          </form>
        </div>
    </div>
    </section>
</div>