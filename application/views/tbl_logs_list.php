
<div class="content-wrapper">
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                  <h3 class='box-title'>Logs Admin 
                </div>
                <div class='box-body'>
                    <?php echo anchor(site_url('admin/logs/excel'), ' <i class="fa fa-file-excel-o"></i> Export Excel', 'class="btn btn-primary btn-sm" style="margin-bottom:10px"'); ?>

        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
            <th>Email</th>
            <th>Message</th>
		    <th>Ip</th>
		    <th>Time</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($logs_data as $logs)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
            <td><?php echo $logs->email ?></td>
            <td><?php echo $logs->message ?></td>
		    <td><?php echo $logs->ip ?></td>
		    <td><?php echo $logs->time ?></td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
                    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

    </div>