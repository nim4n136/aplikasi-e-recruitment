<?php
function cmb_dinamis($name,$table,$field,$pk,$selected=null,$order=null){
    $ci = get_instance();
    $cmb = "<select name='$name' class='form-control'>";
    if($order){
        $ci->db->order_by($field,$order);
    }
    $data = $ci->db->get($table)->result();
    foreach ($data as $d){
        $cmb .="<option value='".$d->$pk."'";
        $cmb .= $selected==$d->$pk?" selected='selected'":'';
        $cmb .=">".  strtoupper($d->$field)."</option>";
    }
    $cmb .="</select>";
    return $cmb;  
}

function select2_dinamis($name,$table,$field,$placeholder){
    $ci = get_instance();
    $select2 = '<select name="'.$name.'" class="form-control select2 select2-hidden-accessible" multiple="" 
    data-placeholder="'.$placeholder.'" style="width: 100%;" tabindex="-1" aria-hidden="true">';
    $data = $ci->db->get($table)->result();
    foreach ($data as $row){
        $select2.= ' <option>'.$row->$field.'</option>';
    }
    $select2 .='</select>';
    return $select2;
}

function datalist_dinamis($name,$table,$field,$value=null){
    $ci = get_instance();
    $string = '<input value="'.$value.'" name="'.$name.'" list="'.$name.'" class="form-control">
    <datalist id="'.$name.'">';
    $data = $ci->db->get($table)->result();
    foreach ($data as $row){
        $string.='<option value="'.$row->$field.'">';
    }
    $string .='</datalist>';
    return $string;
}

function rename_string_is_aktif($string){
    return $string=='y'?'Aktif':'Tidak Aktif';
}


function is_login(){
    $ci = get_instance();
    if(!$ci->session->userdata('id_users')){
        redirect('admin/auth');
    }else{
        $modul = $ci->uri->segment(1).'/'.$ci->uri->segment(2);
        $modul = rtrim($modul, '/');
        $id_user_level = $ci->session->userdata('id_user_level');
        // dapatkan id menu berdasarkan nama controller
        $menu = $ci->db->get_where('tbl_menu',array('url'=>$modul))->row_array();
        $id_menu = $menu['id_menu'];
        // chek apakah user ini boleh mengakses modul ini
        $hak_akses = $ci->db->get_where('tbl_hak_akses',array('id_menu'=>$id_menu,'id_user_level'=>$id_user_level));
        if($hak_akses->num_rows()<1){
            redirect(base_url('index.php/login?lg=blokir'));
            exit;
        }
    }
}

function alert($class,$title,$description){
    return '<div class="alert '.$class.' alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> '.$title.'</h4>
    '.$description.'
    </div>';
}

// untuk chek akses level pada modul peberian akses
function checked_akses($id_user_level,$id_menu){
    $ci = get_instance();
    $ci->db->where('id_user_level',$id_user_level);
    $ci->db->where('id_menu',$id_menu);
    $data = $ci->db->get('tbl_hak_akses');
    if($data->num_rows()>0){
        return "checked='checked'";
    }
}


function autocomplate_json($table,$field){
    $ci = get_instance();
    $ci->db->like($field, $_GET['term']);
    $ci->db->select($field);
    $collections = $ci->db->get($table)->result();
    foreach ($collections as $collection) {
        $return_arr[] = $collection->$field;
    }
    echo json_encode($return_arr);
}


function create_password($password){
    $options        = array("cost"=>4);
    return  password_hash($password,PASSWORD_BCRYPT,$options);
}

function send_email($to, $subject, $message){
    
    $ci = get_instance();
    $config['protocol'] = "smtp";
    $config['smtp_host'] = "ssl://smtp.gmail.com";
    $config['smtp_port'] = "465";
    $config['smtp_user'] = "handihacker33@gmail.com";
    $config['smtp_pass'] = "andi12345@";
    $config['charset'] = "utf-8";
    $config['mailtype'] = "html";
    $config['wordwrap'] = true;

    $ci->load->library('email', $config);
    $ci->email->from('handihacker33@gmail.com', 'Perusahaan');
    $ci->email->set_newline("\r\n");
    $ci->email->to($to);
    $ci->email->subject($subject);
    $ci->email->message($message);
    if ($ci->email->send()) {
        echo 'Email sent.';
    } else {
        show_error($ci->email->print_debugger());
    }
    
    $result = $ci->email->send();

}


function view_template($view, $data = []){
    $ci = get_instance();
    foreach(data_default() as $k => $v)
        $ci->template->set($k, $v);
    $data = array_merge($data, data_default() );
    $ci->template->load('front', 'front/'.$view, $data);     
}


function data_default(){
    return [
        'link_login'    => site_url('login'),
        'link_register' => site_url('register'),
        'organitation_structure' => site_url('organitation_structure'),
        'link_home'     => site_url('/')
    ];
}

function get_avatar($photo = false){
    if($photo)
        return base_url("assets/foto_member/".$photo);
    if(!mysession('photo'))
        return base_url("assets/foto_member/default.png");
    return base_url("assets/foto_member/".mysession('photo'));
}


function load_captcha(){
    $ci = get_instance();
    $option = [
        'img_path'      => './assets/captcha/',
        'img_url'       => base_url('assets/captcha'),
        'img_width' => 270,
        'img_height' => 40,
        'font_size' => 20,
        'pool'          => '0123456789',
        'expiration'    => 7200
    ];
    $cap = create_captcha($option);
    $ci->session->set_userdata('data_captcha', $cap['word']);
    $image = $cap['image'];
    return $image;
}
function mysession($name){
    $ci = get_instance();
    return $ci->session->userdata($name);
}
function set_session($name,$value){
    $ci = get_instance();
    $ci->session->set_userdata($name,$value);
}

function is_lamar(){
    if(mysession('is_lamar'))
        force_redirect('lamar/status');
}

function force_redirect($to){
    redirect($to);exit;
}

function is_login_member($redirect = false){
    if(mysession("id_user") && !mysession("is_complete"))
        force_redirect('info_member');

    if(mysession("id_user") && $redirect)
        force_redirect('home/list');
    if(mysession("id_user")) return true;
}


function info_user($name){
    if(!mysession("is_complete"))
        return;
    return get_info_user($name, mysession("id_user"));
}

function get_info_user($name, $id_member){
    $ci = get_instance();
    $ci->load->model('info_member_model');
    $row = $ci->info_member_model->select(['id_member' =>  $id_member]);
    if(!$row) return;
    if(isset($row->$name))
        return $row->$name;
}

function date_id_en($date, $parse = "/"){
    $exp = explode($parse, $date);
    $result = $exp[2].'-'.$exp[1].'-'.$exp[0];
    return $result;
}


function date_en_id($date, $parse = "-"){
    if(!($date)) return;
    $exp = explode($parse, $date);
    if(!isset($exp[1])) return $date;
    $result = $exp[2].'/'.$exp[1].'/'.$exp[0];
    return $result;
}

function image_slider($image){
    return base_url('assets/foto_slide/'.$image);
}

function get_list_doc($name, $key){
    if(isset(list_doc()[$name][$key]))
        return list_doc()[$name][$key];
}


function list_doc(){
    return [
        'surat_lamaran' => [
            'name' => bahasa('Surat lamaran (pdf)'),
            'ext'  => 'pdf',
            'required' => true,
        ],
        'ktp'           => [
            'name' => bahasa('KTP (png, jpg)'),
            'ext'  => 'png|jpg',
            'required' => true,
        ],
        'sim'           => [
            'name' => bahasa('SIM B2,B1 (pdf)'),
            'ext'  => 'pdf',
            'required' => true,
        ],  
        'ijazah'        => [
            'name' => bahasa('IJAZAH & Transkrip Nilai (pdf)'),
            'ext'  => 'pdf',
            'required' => true,
        ],
        'cv'            => [
            'name'  =>  bahasa('Teofl (pdf)'),
            'ext'   => 'pdf',
            'required' => true,
        ],
        'teofl'         => [
            'name'  => bahasa('Sertifikat Pengalaman Kerja (pdf)'),
            'ext'   => 'pdf',
            'required' => true,
        ],
        'sertifikat'    => [
            'name'  => bahasa('Sertifikat Lain(pdf)'),
            'ext' => 'pdf',
            'required' => true,
        ],
        'sertifikat_lain'   => [
            'name'  =>   bahasa('TEOFL (pdf)'),
            'ext'   => 'pdf',
            'required' => false,
        ]
    ];
}


function save_to_db_list_doc($data){
    return $im = implode("|", (array) $data );
}

function back_to_array_list_doc($data){
    return explode("|", $data);
}

function jenis_kelamin($id){
    if($id == 1)
        return "Laki-Laki";
    else if($id == 2)
        return "Perempuan";
}
function get_data_status($name, $key){
    $data = data_status();
    if(isset($data[$name][$key]))
        return $data[$name][$key];
}

function data_status(){
    return  [
        "pendding" => [
            "name" => "Infromasi",
            "color" => "warning",
        ],
        "accepted_1" => [
            "name" => "Diterima untuk test psikotes",
            "color" => "success" 
        ],
        "accepted_2" => [
            "name" => "Test Psikotes di terima",
            "color" => "success" 
        ],

        "rejected_2" => [
            "name" => "Test Pisikotes Ditolak",
            "color" => "danger",
        ],

        "rejected_1" => [
            "name" => "Lamaran Ditolak",
            "color" => "danger",
        ],
    ];
}

function status($name){
    $label =  get_data_status($name,"name");
    $color =  get_data_status($name,"color");
    return "<div class='label label-{$color}'>{$label}</div>";
}



function status_boost4($name){
    $label =  get_data_status($name,"name");
    $color =  get_data_status($name,"color");
    return "<span class='badge badge-{$color}'>{$label}</span>";
}

function set_bahasa(){
    $ci =& get_instance();
    $ci->load->library('session');
    $site_lang = $ci->session->userdata('site_lang');
    if($site_lang){
        $ci->lang->load('site',$site_lang);
        $ci->lang->load('form',$site_lang);
    }
}
function bahasa($lang){
    $ci = get_instance();
    set_bahasa();
    if($ci->lang->line($lang))
        return $ci->lang->line($lang);
    return $lang;
}

function get_setting($name){
    $ci = get_instance();
    $ci->db->select("value");
    $ci->db->where("nama_setting", $name);
    $query = $ci->db->get("tbl_settings");
    return $query->row_array()['value'];

}

function igorethis(){
     // // $ci = get_instance();
    


    require_once(APPPATH.'third_party/PHPMailer-master/src/Exception.php');
    require_once(APPPATH.'third_party/PHPMailer-master/src/PHPMailer.php');
    require_once(APPPATH.'third_party/PHPMailer-master/src/SMTP.php'); 
    
    $mail = new PHPMailer\PHPMailer\PHPMailer();
    $mail->Mailer = "smtp";
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->Host       = "ssl://smtp.gmail.com";      // setting GMail as our SMTP server
        // $mail->IsSMTP(); // we are going to use SMTP
       // $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        // $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "handihacker33@gmail.com";  // user email address
        $mail->Password   = "andi12345@";            // password in GMail
        $mail->AddReplyTo('handihacker33@gmail.com', 'Perusahaan');  //Who is sending 
        $mail->isHTML(true);
        $mail->Subject    = $subject;
        $mail->Body      = $message;
        $mail->AddAddress($to, "Receiver");
        if(!$mail->Send()) {
            return false;
        } else {
            return true;
        }
    }