<?php 
$lang['error_email_missing'] = 'You must submit an email address';
$lang['error_url_missing'] = 'You must submit a URL';
$lang['error_username_missing'] = 'You must submit a username';
 
/// form
$lang['email_address'] = "Alamat Email";
$lang['full_name']    = "Nama Lengkap";
$lang['no_hp']    = "No HP";
$lang['password']  = "Kata sandi";
$lang['confirm_password'] = "Konfirmasi Kata sandi ";


// form info user
$lang['gender'] = "Jenis Kelamin";
$lang['photo']  = "Foto";
$lang['no_ktp'] = "Nomer KTP";
//form input info user
$lang['gender_1'] = "Laki-Laki";
$lang['gender_2'] = "Perempuan";

$lang['status_1'] = "Menikah";
$lang['status_2'] = "Belum menikah";

$lang['address_1'] = "Alamat Tetap";
$lang['address_2'] = "Alamat sekarang <small>(Masukan alamat sekarang jika berbeda dengan alamat tetap)</small>";
$lang['address_2_1'] = "Alamat sekarang ";

$lang['motivasi'] = "Motivasi";
$lang['insert_motivasi'] = "Silahkan ketik motivasi anda mendaftar rekrutmen ini";

$lang['born'] = "Tempat Lahir";
$lang['born_date'] = "Tanggal Lahir";
$lang['age'] = "Umur";
$lang['status'] = "Status";
$lang['religion'] = "Agama";
$lang['citizen'] = "Warga Negara";
$lang['blood_group'] = "Golongan Darah";
$lang['hight_body'] = "Tinggi Badan";
$lang['weight_body'] = "Berat Badan";

// placeholder
$lang['insert_email_address'] = "Masukan Alamat Email";
$lang['insert_full_name']    = "Masukan Nama Lengkap";
$lang['insert_no_hp']    = "Masukan No HP";
$lang['insert_code_captcha']    = "Masukan kode Captcha ";
$lang['insert_password'] = "Masukan Kata sandi";
$lang['insert_confirm_password'] = "Masukan Konfirmasi Kata sandi ";
$lang['insert_gender'] = "Masukan jenis kelamin";
$lang['insert_no_ktp'] = "Masukan Nomer KTP";
$lang['insert_born'] = "Masukan Tempat Lahir";
$lang['insert_born_date'] = "Masukan Tanggal Lahir";
$lang['insert_age'] = "Masukan Umur (Tahun)";
$lang['choice_status'] = "Pilih status anda";
$lang['choice_gender'] = "Pilih jenis kelamin";
$lang['choose_photo'] = "Pilih Foto";
$lang['choice_religion'] = "Pilih Agama";
$lang['insert_citizen'] = "Masukan Warga Negara";
$lang['insert_address_1'] = "Masukan Alamat Tetap";
$lang['insert_address_2'] = "Masukan Alamat Sekarang";
$lang['insert_blood_group'] = "Masukan Golongan Darah";

$lang['insert_hight_body'] = "Masukan Tinggi Badan (CM)";
$lang['insert_weight_body'] = "Masukan Berat Badan (KG)";

// Button
$lang['register_now'] = "Daftar Sekarang";
$lang['register'] = "Daftar";
$lang['login'] = "Masuk";
$lang['submit'] = "Submit";
$lang['save'] = "Simpan";

// title
$lang['register_account'] = "Daftar Akun";
$lang['create_new_password'] = "Kata sandi baru";


// error
$lang['sorry_captcha_is_wrong'] = "Silahkan masukan Captcha dengan benar";
$lang['email_address_already_use'] = "Maaf!! Email yang anda masukan sudah di gunakan";
$lang['failed_to_created_new_password'] = "Terjadi kesalahan dalam membuat kata sandi baru";
$lang['token_has_been_expire'] = "Token telah kadaluarsa";
$lang['email_not_found'] = "Email anda belum terdaftar";
$lang['wrong_password'] = "Password yang kamu masukan salah";
$lang['email_not_yet_confirm'] = "Alamat email belum di konfirmasi";


// info
$lang['create_new_password_for_active_account'] = "Buatlah kata sandi baru untuk mengaktifkan akun";
$lang['check_inbox_email_to_activate_the_account'] = "Silahkan cek email untuk mengaktifkan akun";
$lang['confirm_email_success_please_login'] = "Akun telah di aktifkan silahkan login untuk melanjutkan";
$lang['complete_info_user'] = "Lengkapi Biodata Anda";
$lang['file_photo_required'] = "Foto belum di tambahkan silahkan tambahkan";
