<?php 
$lang['error_email_missing'] = 'You must submit an email address';
$lang['error_url_missing'] = 'You must submit a URL';
$lang['error_username_missing'] = 'You must submit a username';
 
/// form
$lang['email_address'] = "Email Address";
$lang['full_name']    = "Full Name";
$lang['no_hp']    = "No Handphone";
$lang['password']  = "Password";
$lang['confirm_password'] = "Password Confirmation";


// form info user
$lang['gender'] = "Gender";
$lang['photo']  = "Photo";
$lang['no_ktp'] = "ID card number";

//form input info user
$lang['gender_1'] = "Male";
$lang['gender_2'] = "Female";

$lang['status_1'] = "Married";
$lang['status_2'] = "Single";

$lang['address_1'] = "Permanent Address";
$lang['address_2'] = "Current address <small> (Enter the current address if it is different from a fixed address) </small>";
$lang['address_2_1'] = "Current address";

$lang['born'] = "Place of birth";
$lang['born_date'] = "Date of birth";
$lang['age'] = "Age";
$lang['status'] = "Status";
$lang['religion'] = "Religion";
$lang['citizen'] = "Citizen";
$lang['blood_group'] = "Blodd group";
$lang['hight_body'] = "Hight body";
$lang['weight_body'] = "Weight body";
$lang['motivasi'] = "Motivation";
$lang['insert_motivasi'] = "Please enter your motivation to register";

// placeholder
$lang['insert_email_address'] = "Enter Email Address";
$lang['insert_full_name']    = "Enter Full Name";
$lang['insert_no_hp']    = "Enter No Handphone";

$lang['insert_code_captcha']    = "Enter Code Captcha";
$lang['insert_password'] = "Enter Password";
$lang['insert_confirm_password'] = "Re-enter comfirm password ";
$lang['insert_gender'] = "Choice Gender";
$lang['insert_no_ktp'] = "Enter ID Card Number";
$lang['insert_born'] = "Enter place of birth";
$lang['insert_born_date'] = "Enter date of birth";
$lang['insert_age'] = "Enter age (Year)";
$lang['choice_status'] = "Choice your status";
$lang['choice_gender'] = "Choice gender";
$lang['choose_photo'] = "Choice photo";
$lang['choice_religion'] = "Choice Religion";
$lang['insert_citizen'] = "Enter Citizen";



$lang['insert_address_1'] = "Enter Permanent Address";
$lang['insert_address_2'] = "Enter Current address";
$lang['insert_blood_group'] = "Enter Blood Group";

$lang['insert_hight_body'] = "Enter hight body (CM)";
$lang['insert_weight_body'] = "Enter width body (KG)";

// Button
$lang['register_now'] = "Register Now";
$lang['register'] = "Register";
$lang['login'] = "Login";
$lang['submit'] = "Submit";
$lang['save'] = "Save";

// title
$lang['register_account'] = "Register Account";
$lang['create_new_password'] = "New Password";


// error
$lang['sorry_captcha_is_wrong'] = "Please enter Captcha correctly";
$lang['email_address_already_use'] = "Sorry!! The email you entered has been used";
$lang['failed_to_created_new_password'] = "An error occurred while creating a new password";
$lang['token_has_been_expire'] = "The token has expired";
$lang['email_not_found'] = "Your email has not been registered";
$lang['wrong_password'] = "The password you entered is incorrect";
$lang['email_not_yet_confirm'] = "Email address has not been confirmed";


// info
$lang['create_new_password_for_active_account'] = "Create a new password to activate the account";
$lang['check_inbox_email_to_activate_the_account'] = "Please check the email to activate the account";
$lang['confirm_email_success_please_login'] = "The account has been activated, please login to continue";
$lang['complete_info_user'] = "Complete your Biodata";
$lang['file_photo_required'] = "Photos have not been added, please add";
