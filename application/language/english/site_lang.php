<?php 

$lang['Anda telah mengirimkan data pelamar'] = "You have sent applicant data";
$lang['Selamat datang di website - E-Recruitment'] = "Welcome to the website - E-Recruitment";
$lang['Semua lowongan pekerjaan'] = "All job openings";
$lang['Lihat & Lamar'] = "Detail & Apply";
$lang['Cari lowongan pekerjaan'] = "Search for job openings";
$lang['Cari Pekerjaan'] = "Search jobs";
$lang['Cari'] = "Search";
$lang['Tahun'] = "Year";
$lang['Pengalaman'] = "Experience";
$lang['Bahasa'] = "Language";

$lang['JOBS DI'] = "JOBS IN";
$lang['Lihat semua lowongan'] = "See all Job openings";
$lang['Masuk'] = "Login";
$lang['Daftar'] = "Register";

$lang['Download hasil test psikotes'] = "Download the results of the psychological test";
$lang['Silahkan print / save dokumen untuk mengikuti psikotes'] = "Please print / save the document to follow the psychological test";
$lang['Informasi'] = "Information";
$lang['Status'] = "Status";
$lang['Posisi / Nama Pekerjaan'] = "Position / Job Name";
$lang['Status Lamaran anda'] = "Status of your application";

$lang['Informasi semua status pelamar'] = "Information on all applicants' status";
$lang['Nama Lengkap'] = "Full Name";
$lang['Semua Informasi'] = "All information";
$lang['Silahkan masuk'] = "Login now";

$lang['Lamar'] = "Apply";
$lang['Deskripsi Pekerjaan'] = "Job description";