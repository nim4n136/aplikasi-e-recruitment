-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 04 Agu 2019 pada 00.40
-- Versi server: 10.3.15-MariaDB-1
-- Versi PHP: 7.3.4-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_lamar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `config_rn`
--

CREATE TABLE `config_rn` (
  `id` int(15) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `config_rn`
--

INSERT INTO `config_rn` (`id`, `judul`, `isi`, `tanggal`, `id_user`) VALUES
(1, 'Struktur organisasi', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;</p>\r\n', '2019-06-18', '1'),
(2, 'TES', 'TES', '2019-04-03', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_data`
--

CREATE TABLE `tbl_data` (
  `id` int(11) NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_data`
--

INSERT INTO `tbl_data` (`id`, `id_member`, `name`, `value`) VALUES
(1, 22, 'ktp', 'ktp-niman-58f7e76d8251224da73e0cc9cdc6d0a55f71add5.png'),
(2, 22, 'surat_lamaran', 'surat_lamaran-niman-668157b7239bcc380d46c9d2868fb598459b1394.pdf'),
(3, 23, 'ktp', 'ktp-niman-second-d20b63e3beb82891a7b182775356c8c9d780802e.png'),
(4, 23, 'surat_lamaran', 'surat_lamaran-niman-second-c65e857cbbc9f2fdedcb4285bb98830ab9fae9a2.pdf'),
(5, 13, 'teofl', 'teofl-niman-1891abc945cd72a8ff252ca2c75080d11c1d03d0.pdf'),
(6, 13, 'ijazah', 'ijazah-niman-bda8bbdd01ca1c165d62e5ab12b1cd51f790be6e.pdf'),
(7, 13, 'ktp', 'ktp-niman-a1e2b29cf49c04748b53047b0d2ebad684a319d1.jpg'),
(8, 13, 'surat_lamaran', 'surat_lamaran-niman-9640b550b4a47c97eee488f0b151a292767a9a6d.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hak_akses`
--

CREATE TABLE `tbl_hak_akses` (
  `id` int(11) NOT NULL,
  `id_user_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_hak_akses`
--

INSERT INTO `tbl_hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES
(15, 1, 1),
(19, 1, 9),
(21, 2, 0),
(22, 1, 8),
(29, 2, 1),
(30, 2, 9),
(31, 1, 7),
(32, 1, 6),
(34, 2, 2),
(35, 1, 5),
(36, 1, 4),
(37, 1, 3),
(39, 1, 11),
(41, 1, 10),
(42, 1, 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_info_member`
--

CREATE TABLE `tbl_info_member` (
  `id` int(11) NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `file_kemampuan` varchar(50) NOT NULL,
  `no_ktp` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `umur` int(100) DEFAULT NULL,
  `jenis_kelamin` varchar(30) DEFAULT NULL,
  `status` varchar(120) DEFAULT NULL,
  `pendidikan` varchar(255) DEFAULT NULL,
  `tahun_lulus` varchar(255) DEFAULT NULL,
  `jurusan` varchar(255) DEFAULT NULL,
  `warga_negara` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `gol_darah` varchar(50) DEFAULT NULL,
  `alamat_tetap` varchar(255) DEFAULT NULL,
  `alamat_sekarang` varchar(255) DEFAULT NULL,
  `tinggi_badan` varchar(50) DEFAULT NULL,
  `berat_badan` varchar(50) DEFAULT NULL,
  `motivasi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_info_member`
--

INSERT INTO `tbl_info_member` (`id`, `id_member`, `photo`, `file_kemampuan`, `no_ktp`, `tempat_lahir`, `tanggal_lahir`, `umur`, `jenis_kelamin`, `status`, `pendidikan`, `tahun_lulus`, `jurusan`, `warga_negara`, `agama`, `gol_darah`, `alamat_tetap`, `alamat_sekarang`, `tinggi_badan`, `berat_badan`, `motivasi`) VALUES
(13, 13, 'profile.jpg', 'profile.jpg', '11212121212121', 'dsadsa', '22/08/2019', 11, '1', '1', 'dsadsa', '03/08/2019', 'dasd', 'Indonesia', 'Islam', 'dsadsa', 'Jakarta indinesia', 'Jakarta indinesia', '1112', '11', 'dsadsad');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_list_jobs`
--

CREATE TABLE `tbl_list_jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_active` date DEFAULT NULL,
  `date_expire` date DEFAULT NULL,
  `max_age` int(100) DEFAULT NULL,
  `academic` varchar(255) DEFAULT NULL,
  `experience` int(100) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `doc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_list_jobs`
--

INSERT INTO `tbl_list_jobs` (`id`, `name`, `date_active`, `date_expire`, `max_age`, `academic`, `experience`, `salary`, `description`, `doc`) VALUES
(2, 'Programmer C++ dan Expert JAVA', '2011-09-11', '2019-11-11', 2, 'S1', 2, '20000000', 'dsad', 'surat_lamaran|ktp|ijazah|teofl'),
(4, 'IT Support', '1999-07-07', '2018-12-29', 11, 'dasd', 12, '1000000', '<p>tryuiop\'asjfjsaf afshasf<br></p>', 'sim|ijazah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_logs`
--

CREATE TABLE `tbl_logs` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `ip` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_logs`
--

INSERT INTO `tbl_logs` (`id`, `email`, `message`, `ip`, `time`) VALUES
(1, 'super_admin@gmail.com', 'Melakukan login', '127.0.0.1', '9-03-2019 14:06:55'),
(2, 'super_admin@gmail.com', 'Melakukan login', '127.0.0.1', '9-03-2019 14:09:10'),
(3, 'super_admin@gmail.com', 'Melakukan login', '127.0.0.1', '9-03-2019 14:09:17'),
(4, 'super_admin@gmail.com', 'Melakukan login', '127.0.0.1', '9-03-2019 14:10:12'),
(5, 'super_admin@gmail.com', 'Melakukan login', '127.0.0.1', '9-03-2019 14:10:18'),
(6, 'super_admin@gmail.com', 'Melakukan logout', '127.0.0.1', '9-03-2019 14:10:22'),
(7, 'super_admin@gmail.com', 'Melakukan login', '127.0.0.1', '9-03-2019 14:10:27'),
(8, 'super_admin@gmail.com', 'Melakukan login', '::1', '22-03-2019 05:36:43'),
(9, 'super_admin@gmail.com', 'Melakukan login', '::1', '22-03-2019 08:31:53'),
(10, 'super_admin@gmail.com', 'Melakukan login', '::1', '22-03-2019 10:42:12'),
(11, 'super_admin@gmail.com', 'Melakukan login', '::1', '24-03-2019 09:14:17'),
(12, 'super_admin@gmail.com', 'Melakukan login', '::1', '25-03-2019 01:49:53'),
(13, 'super_admin@gmail.com', 'Melakukan login', '::1', '25-03-2019 04:26:12'),
(14, 'super_admin@gmail.com', 'Melakukan logout', '::1', '25-03-2019 04:28:26'),
(15, 'super_admin@gmail.com', 'Melakukan login', '::1', '25-03-2019 04:28:32'),
(16, 'super_admin@gmail.com', 'Melakukan login', '::1', '31-03-2019 13:51:57'),
(17, 'super_admin@gmail.com', 'Melakukan login', '127.0.0.1', '31-03-2019 17:24:01'),
(18, 'super_admin@gmail.com', 'Melakukan logout', '127.0.0.1', '31-03-2019 20:01:07'),
(19, 'moko@gmail.com', 'Melakukan login', '127.0.0.1', '31-03-2019 20:02:58'),
(20, 'moko@gmail.com', 'Melakukan login', '::1', '01-04-2019 16:15:10'),
(21, 'moko@gmail.com', 'Melakukan login', '::1', '03-04-2019 17:18:52'),
(22, 'moko@gmail.com', 'Melakukan logout', '::1', '03-04-2019 18:14:14'),
(23, 'moko@gmail.com', 'Melakukan login', '::1', '03-04-2019 18:14:53'),
(24, 'moko@gmail.com', 'Melakukan logout', '::1', '03-04-2019 18:15:11'),
(25, 'super_admin@gmail.com', 'Melakukan login', '::1', '03-04-2019 18:15:16'),
(26, 'super_admin@gmail.com', 'Melakukan logout', '::1', '03-04-2019 18:24:32'),
(27, 'super_admin@gmail.com', 'Melakukan login', '::1', '03-04-2019 18:25:30'),
(28, 'super_admin@gmail.com', 'Melakukan logout', '::1', '03-04-2019 18:27:38'),
(29, 'super_admin@gmail.com', 'Melakukan login', '::1', '03-04-2019 18:28:27'),
(30, 'super_admin@gmail.com', 'Melakukan logout', '::1', '03-04-2019 18:31:31'),
(31, 'super_admin@gmail.com', 'Melakukan login', '::1', '03-04-2019 18:31:55'),
(32, 'super_admin@gmail.com', 'Melakukan login', '::1', '03-04-2019 18:35:57'),
(33, 'super_admin@gmail.com', 'Melakukan login', '103.124.91.51', '28-05-2019 00:57:07'),
(34, 'super_admin@gmail.com', 'Melakukan logout', '103.124.91.51', '28-05-2019 00:57:14'),
(35, 'super_admin@gmail.com', 'Melakukan login', '103.124.91.51', '28-05-2019 01:11:03'),
(36, 'super_admin@gmail.com', 'Melakukan login', '103.124.91.51', '28-05-2019 09:49:37'),
(37, 'super_admin@gmail.com', 'Melakukan login', '36.68.236.185', '28-05-2019 14:02:01'),
(38, 'super_admin@gmail.com', 'Melakukan login', '114.125.253.31', '29-05-2019 15:42:07'),
(39, 'super_admin@gmail.com', 'Melakukan login', '182.1.21.228', '03-06-2019 16:54:49'),
(40, 'super_admin@gmail.com', 'Melakukan login', '103.124.91.51', '18-06-2019 01:20:56'),
(41, 'super_admin@gmail.com', 'Melakukan login', '::1', '18-06-2019 05:33:36'),
(42, 'super_admin@gmail.com', 'Melakukan login', '::1', '28-06-2019 17:29:47'),
(43, 'super_admin@gmail.com', 'Melakukan login', '::1', '30-06-2019 14:21:35'),
(44, 'super_admin@gmail.com', 'Melakukan login', '::1', '12-07-2019 18:10:43'),
(45, 'super_admin@gmail.com', 'Melakukan login', '::1', '15-07-2019 15:54:55'),
(46, 'super_admin@gmail.com', 'Melakukan login', '::1', '15-07-2019 16:18:40'),
(47, 'super_admin@gmail.com', 'Melakukan login', '::1', '15-07-2019 18:47:39'),
(48, 'super_admin@gmail.com', 'Melakukan login', '::1', '21-07-2019 12:51:04'),
(49, 'super_admin@gmail.com', 'Melakukan login', '::1', '22-07-2019 17:46:16'),
(50, 'super_admin@gmail.com', 'Melakukan logout', '::1', '22-07-2019 18:27:18'),
(51, 'super_admin@gmail.com', 'Melakukan login', '::1', '27-07-2019 16:55:42'),
(52, 'super_admin@gmail.com', 'Melakukan login', '::1', '31-07-2019 07:16:42'),
(53, 'super_admin@gmail.com', 'Melakukan login', '::1', '01-08-2019 17:39:48'),
(54, 'super_admin@gmail.com', 'Melakukan login', '::1', '04-08-2019 00:19:27'),
(55, 'super_admin@gmail.com', 'Melakukan logout', '::1', '04-08-2019 00:33:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_member`
--

CREATE TABLE `tbl_member` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `no_handphone` varchar(255) DEFAULT NULL,
  `verify_email` enum('1','0') NOT NULL DEFAULT '0',
  `kode_verify` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_member`
--

INSERT INTO `tbl_member` (`id`, `full_name`, `email`, `password`, `no_handphone`, `verify_email`, `kode_verify`) VALUES
(1, 'Edward', 'edward@sds.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '24242', '1', 'aa14512ad388fb38c03e4f307718f29714f3670f'),
(12, 'Harmoko', 'isafesbs@gmail.com', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', '081273328123', '1', 'eb93c170901f41488c360267369fc0e501d84436'),
(13, 'Niman1', 'nim4n131@gmail.com', '$2y$04$wSUUnOIGpaXVM4FhAUCfyOqSWobzf1K.Qoy1nRDe7Nv6qjbnSeYjy', '090909090', '1', 'c022857f0c06150054ca38f895fd9e5f9d515ebd'),
(15, 'asdasdsad', 'nim4n136@gmail.com', '$2y$04$w91N/Z9dLPki/zeALZWEsu29FrbqeWdo6Zv4l2EkWKIvpzj/ICGwq', '0822102233311', '0', '3606ffa1854f1178eb03552b39d2b9afe96ab7b3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `url` varchar(30) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_main_menu` int(11) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL COMMENT 'y=yes,n=no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`) VALUES
(1, 'Dashboard', 'admin', 'fa fa-dashboard', 0, 'y'),
(2, 'Account Pelamar', 'admin/member', 'fa fa-users', 0, 'y'),
(3, 'Kelola Admin', 'admin/user', 'fa fa-user-o', 0, 'y'),
(4, 'Level Pengguna', 'admin/userlevel', 'fa fa-users', 0, 'y'),
(5, 'Kelola Menu', 'admin/kelolamenu', 'fa fa-server', 0, 'y'),
(6, 'Daftar Jobs', 'admin/jobs_list', 'fa fa-briefcase', 0, 'y'),
(7, 'Silder', 'admin/slider', 'fa fa-cog', 8, 'y'),
(8, 'Settings', '#', 'fa fa-cog', 0, 'y'),
(9, 'Data Pelamar', 'admin/status_member', 'fa fa-users', 0, 'y'),
(10, 'Umum', '/admin/settings', 'fa fa-cog', 8, 'y'),
(11, 'Ganti Password', '/admin/user', 'fa fa-cog', 0, 'y'),
(12, 'Logs Admin', '/admin/logs', 'fa fa-users', 0, 'y'),
(15, 'Data  Halaman', '/halaman/managable', 'fa fa-list', 0, 'y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id_setting` int(11) NOT NULL,
  `nama_setting` varchar(50) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_settings`
--

INSERT INTO `tbl_settings` (`id_setting`, `nama_setting`, `value`) VALUES
(1, 'Tampil Menu', 'ya'),
(2, 'lang_default', 'en'),
(3, 'email', 'recruit@sbs.com'),
(4, 'brand', 'JOBS PT. SB'),
(5, 'phone', '+62 81273328123'),
(6, 'address', 'Jln.'),
(7, 'facebook', 'facebook.com'),
(8, 'youtube', 'youtube.com'),
(9, 'twitter', 'JOBS PT. S'),
(10, 'about', 'Pert'),
(11, 'new', 'fghj');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `lang` enum('id','en') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `pic`, `title`, `description`, `lang`) VALUES
(4, '222a33f518c910b5fd0dd75eacd69b1a164d0ab5-1-slide.jpg', 'Karir bersama kami PT.  SBS', 'id', 'id'),
(5, '543807790a9ff152b6cd7869212053207ab98ec9-2-slide.jpg', 'One more for good measure.', 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'id'),
(6, '222a33f518c910b5fd0dd75eacd69b1a164d0ab5-1-slide.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'en'),
(7, 'a62c5eeeef23f023efe240838b972b3fad26d3d6-head.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit', 'en', 'en');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_status_member`
--

CREATE TABLE `tbl_status_member` (
  `id` int(11) NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_jobs` int(11) DEFAULT NULL,
  `status` enum('accepted_1','pendding','rejected_1','accepted_2','rejected_2') DEFAULT NULL,
  `file_psikotes` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_status_member`
--

INSERT INTO `tbl_status_member` (`id`, `id_member`, `id_jobs`, `status`, `file_psikotes`) VALUES
(3, 22, 2, 'accepted_2', '{\"file_name\":\"file_psikotes-677e559fc392d99368628b0d484d3dc39211ea04.xls\",\"keterangan\":\"Anda ikut test selanjutnya pada tanggal 02\\/12\\/2018\"}'),
(4, 23, 2, 'accepted_2', '{\"file_name\":\"file_psikotes-725b5aa7db87fb77f462495191089cfe1b6ff155.xls\",\"keterangan\":\"Anda ikut test selanjutnya pada tanggal 02\\/12\\/2018\"}'),
(5, 13, 2, 'pendding', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_users` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `id_user_level` int(11) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id_users`, `full_name`, `email`, `password`, `images`, `id_user_level`, `is_aktif`) VALUES
(1, 'Super Admin', 'super_admin@gmail.com', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 1, 'y'),
(2, 'moko', 'moko@gmail.com', '$2y$10$vCDeM/G6kf45YkgWoqjrf.v1PvbmINt82oIccHtxom1djCVVDYJAa', 'pf1554132347.jpg', 2, 'y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user_level`
--

CREATE TABLE `tbl_user_level` (
  `id_user_level` int(11) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user_level`
--

INSERT INTO `tbl_user_level` (`id_user_level`, `nama_level`) VALUES
(1, 'Super Admin'),
(2, 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `config_rn`
--
ALTER TABLE `config_rn`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_data`
--
ALTER TABLE `tbl_data`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hak_akses`
--
ALTER TABLE `tbl_hak_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_info_member`
--
ALTER TABLE `tbl_info_member`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_list_jobs`
--
ALTER TABLE `tbl_list_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indeks untuk tabel `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_status_member`
--
ALTER TABLE `tbl_status_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id_jobs`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_users`);

--
-- Indeks untuk tabel `tbl_user_level`
--
ALTER TABLE `tbl_user_level`
  ADD PRIMARY KEY (`id_user_level`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `config_rn`
--
ALTER TABLE `config_rn`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_data`
--
ALTER TABLE `tbl_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_hak_akses`
--
ALTER TABLE `tbl_hak_akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `tbl_info_member`
--
ALTER TABLE `tbl_info_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `tbl_list_jobs`
--
ALTER TABLE `tbl_list_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT untuk tabel `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tbl_status_member`
--
ALTER TABLE `tbl_status_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_user_level`
--
ALTER TABLE `tbl_user_level`
  MODIFY `id_user_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
